# Kubernetes

## Managing users

[User creation](https://github.com/kubernetes/dashboard/blob/master/docs/user/access-control/creating-sample-user.md)


## Managing images

[Images]{https://kubernetes.io/docs/concepts/containers/images/}

## Application

- [Run a Stateless Application Using a Deployment](https://v1-17.docs.kubernetes.io/docs/tasks/run-application/run-stateless-application-deployment/)

## Secrets

- [Kubernetes -POD with ssh keys](https://v1-17.docs.kubernetes.io/docs/concepts/configuration/secret/)
- [Just me and opensourece Youtube - using secretes](https://www.youtube.com/watch?v=ch9YlQZ4xTc&t=879s)

## Cronjobs

Quote:

A K8 cronjob is a scheculed container running within a POD. A Container is guaranteed to have as much memory as it requests, but is not allowed to use more memory than its limit. A Container can exceed its memory request if the Node has memory available. But a Container is not allowed to use more than its memory limit. If a Container allocates more memory than its limit, the Container becomes a candidate for termination. If the Container continues to consume memory beyond its limit, the Container is terminated. If a terminated Container can be restarted, the kubelet restarts it, as with any other type of runtime failure.

Procedure for creating a cronjob:

- Create Docker image with code for your application

- Push image to a public or private repository

- Create a config file

-

## Delete deployment

```kubectl delete deployment nginx```

This will forseable delete PODS.

### Links

- [Running automated scheduled tast with cronjobs](https://v1-17.docs.kubernetes.io/docs/tasks/job/automated-tasks-with-cron-jobs/)

- [Cronjob concepts](https://kubernetes.io/docs/concepts/workloads/controllers/cron-jobs/)

- [Assign memory resource to containers and pods](https://v1-17.docs.kubernetes.io/docs/tasks/configure-pod-container/assign-memory-resource/)

- [Troubleshooting Kubernetes](https://managedkube.com/kubernetes/k8sbot/troubleshooting/imagepullbackoff/2019/02/23/imagepullbackoff.html)

- [Kubernetes the hard way](https://github.com/kelseyhightower/kubernetes-the-hard-way)