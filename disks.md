# Disk

This file documents how to 
- add
- tune
- remove

disk operations

## Add new disk

This method will use [parted](https://github.com/Distrotech/parted).

Add parted if not already to your operating system

### Install parted
`sudo apt-get install parted   # For Debian/Ubuntu

or

sudo yum install parted       # For Red Hat/CentOS
`
### assign a label

`parted --script /dev/sdb mklabel gpt` - assigns a GPT label

### Create a primary partition

`parted /dev/sdb mkpart primary ext4 0% 100%` - creates a primary partion using the complete drive space.

The disk has now been partitioned and available to be used

## Create a filesystem on the drive

Once the drive has been partition you need to install a filesystem. The mkfs command is used to make a file system on a device, usually a partition on a hard disk drive (HDD), or it can also be a USB

`mkfs.ext4 /dev/sdb1` 

File System Choice:
Choose an appropriate file system based on your use case. For general-purpose usage, ext4 is a common and stable choice. If you have specific requirements (e.g., large files, flash drives), you might consider other file systems like XFS or Btrfs.

## Tuning the drive

I/O Scheduler:
Adjust the I/O scheduler to better suit your workload. Common schedulers include cfq, deadline, and noop. You can change the scheduler for a specific block device:

`echo "deadline" | sudo tee /sys/block/sdb/queue/scheduler`

### File System Features
If you're using a file system like ext4, you can enable or disable certain features based on your needs. For example, you might enable or disable the journaling feature based on performance considerations.

### Disable journaling
sudo tune2fs -O ^has_journal /dev/sdb1

### Enable journaling
sudo tune2fs -o journal_data /dev/sdb1

### Swap Configuration
If your drive includes swap space, you might consider adjusting the swappiness value to control the tendency of the system to use swap space. Lowering the swappiness value can reduce disk I/O.

### Check current swappiness value
cat /proc/sys/vm/swappiness

### Set a new swappiness value (e.g., 10)
`echo "10" | sudo tee /proc/sys/vm/swappiness`
Read-Ahead Buffer:
Adjust the read-ahead buffer size to optimize sequential read performance. This can be done using the blockdev command.

### Set read-ahead buffer size (e.g., 8192 sectors)
`sudo blockdev --setra 8192 /dev/sdb`

Solid State Drive (SSD) Specific Tweaks:
If the new drive is an SSD, consider additional optimizations such as enabling TRIM support. Most modern Linux distributions automatically enable TRIM for SSDs, but you may want to confirm this.

### Check TRIM status
`sudo systemctl status fstrim.timer`
This command checks the status of the fstrim service, which is responsible for sending TRIM commands to the SSD.

## Mount the drive

To be able to mount the drive you need to create a mount point e.g `mkdir /data` /data would be the mount point. 

`mount /dev/sdb1 /data`

If you want to allow a normal user to create files on this drive, you can either give this user ownership of the top directory of the drive filesystem: (replace USERNAME with the actual username).

`chown -R pbmac:pbmac /data`
Or, in a more flexible way and practical if you have several users, allow for instance the users in the proj2020 group (usually those who are meant to be able to mount removable disks, desktop users) to create files and sub-directories on the disk:

 `chgrp proj2020 /data`
 `chmod g+w /datae`
 `chmod +t /data`

The last chmod +t adds the sticky bit, so that people can only delete their own files and sub-directories in a directory, even if they have write permissions to it (see man chmod).

Once you have the mount point, mount the drive using the following command:

### update /etc/fstab

Obtain the UUID for the new disk partition (/dev/sdb1) using the blkid command
`blkid /dev/sdb1`

e.g `/dev/sdb1: UUID="4de40198-23b7-4bfc-afc2-7bde0530bfcc" TYPE="ext4" PARTLABEL="primary" PARTUUID="b9f7c187-da29-414e-8a9e-a064b95db3bb"`

uuid = UUID="4de40198-23b7-4bfc-afc2-7bde0530bfcc"

add the following to the /etc/fstab

`UUID="4de40198-23b7-4bfc-afc2-7bde0530bfcc" /data ext4 defaults 0 1`

explanation:

UUID="4de40198-23b7-4bfc-afc2-7bde0530bfcc":
This is the Universally Unique Identifier (UUID) of the filesystem. UUIDs are used to uniquely identify filesystems and are preferred over device names (e.g., /dev/sdX) because they remain consistent even if the drive order changes.

/data:
This is the mount point where the filesystem will be attached. In this case, it is mounted at the /data directory. The mount point is the location in the filesystem hierarchy where the contents of the new drive will be accessible.

ext4:
This specifies the filesystem type. In this case, it's ext4, which is a widely used and robust filesystem type for Linux.

defaults:
These are the mount options for the filesystem. The defaults keyword typically includes options like rw (read-write), suid (set user ID), dev (interpret character or block special devices), exec (allow execution of binaries), auto (automount at boot), and nouser (disallow ordinary users to mount).

0:
This field is used by the dump command to determine whether the filesystem should be backed up. A value of 0 indicates that the filesystem should not be included in the backup.

1:
This field is used by the fsck command to determine the order in which filesystem checks are done at boot time. A value of 1 indicates that the filesystem should be checked first.

## remove a drive

- unmount the drive
`umount /data` then remove the mount point in /etc/fstab

Either remove the physical drive ( power off and remove) or virtual drive.


