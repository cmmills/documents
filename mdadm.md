# MDADM

mdadm keeps a list of arrays that it has partially assembled in /dev/md/md-device-map. 

mdadm -Ac partitions -m 0 /dev/md0
Scan all partitions and devices listed in /proc/partitions and assemble /dev/md0 out of all such devices with a RAID superblock with a minor number of 0. 

Files

/proc/mdstat

If you're using the /proc filesystem, /proc/mdstat lists all active md devices with information about them. mdadm uses this to find arrays when --scan is given in Misc mode, and to monitor array reconstruction on Monitor mode.

/etc/mdadm.conf

The config file lists which devices may be scanned to see if they contain MD super block, and gives identifying information (e.g. UUID) about known MD arrays. See mdadm.conf(5) for more details.

/dev/md/md-device-map

When --incremental mode is used, this file gets a list of arrays currently being created.

links 

- [MDADM](https://linux.die.net/man/8/mdadm)
- [A guide to mdadm](https://raid.wiki.kernel.org/index.php/A_guide_to_mdadm)
- [Setting up raid](https://raid.wiki.kernel.org/index.php/Setting_up_a_(new)_system)
- [Monitoring your system](https://raid.wiki.kernel.org/index.php/Monitoring_your_system)

possible solutions:

- [/dev/md0 missing](https://serverfault.com/questions/770332/mdadm-software-raid-isnt-assembled-at-boot-during-initramfs-stage)

Related:

- [Troubleshooting MDADM on debian][https://superuser.com/questions/287462/how-can-i-make-mdadm-auto-assemble-raid-after-each-boot]
