# Using influxdb

## Connecting to influxdb

- ``` influx -precision rfc3339```


## Show all databases

- ```show databases```

## Use a database

- ```use ansible_playbook```

## Deleting a measurement.

### Delete a measurement

- ```drop measurement playstats```

### Links

- [ Influx basics](https://docs.influxdata.com/influxdb/v1.7/query_language/database_management/)
- [Exploring data in influxdb](https://docs.influxdata.com/influxdb/v1.7/query_language/data_exploration/#the-basic-select-statement)
- [writing data to influxdb](https://docs.influxdata.com/influxdb/v1.7/guides/writing_data/)