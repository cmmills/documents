# Package mangement

Ubuntu systems can use either apt or dpkg for installing applications on Ubuntu based systems

Dpkg
find information about a package
```dpkg -I (capital I) <debian package name>```
```dpkg -I zabbix-agent_2.2.4+precise_amd64.deb```

## Installing .deb packages using dpkg

```dpkg -i <debian package name>```
```dpkg -i zabbix-agent_2.2.4-1+precise_amd64.deb```

## Remove a .deb package once it's installed using dpkg

Before you can remove the package, you need to know what you are trying to remove. if you still have the .deb file, you can use the following command.

```dpkg-deb --show zabbix-agent_2.2.4+precise_amd64.deb```

This will give you the name of the installed software

```shell
root@srv3:~# dpkg-deb --show /tmp/zabbix-agent_2.2.4-1+precise_amd64.deb 
zabbix-agent	1:2.2.4-1+precise
```

## Removing a .deb package using apt

The other method is to use the apt command more precisely.

```shell
apt list --installed | grep apache

root@srv3:~# apt list --installed | grep -i zabbix

WARNING: apt does not have a stable CLI interface yet. Use with caution in scripts.

zabbix-agent/trusty,trusty,now 1:2.2.2+dfsg-1ubuntu1 armhf [installed]
```

Once you know the name of the software you can use the following commands to remove the software.

```shell
dpkg -r zabbix-agent
or
apt-get purge zabbix-agent
```

## list installed software

```apt list --installed```

or


```dpkg --get-selections```

## List packages starting with ansible

```shell
apt-cache pkgnames ansible
```

## Show available versions of a package that can be installed.

apt-cache show ansible

```shell
deployuser@test-srv-1:~$ apt-cache show ansible
Package: ansible
Architecture: all
Version: 1.7.2+dfsg-1~ubuntu14.04.1
Priority: optional
Section: universe/admin
Origin: Ubuntu
Maintainer: Ubuntu Developers <ubuntu-devel-discuss@lists.ubuntu.com>
Original-Maintainer: Janos Guljas <janos@debian.org>
Bugs: https://bugs.launchpad.net/ubuntu/+filebug
Installed-Size: 3400
Depends: python (>= 2.7), python (<< 2.8), python:any (>= 2.7.1-0ubuntu2), python-paramiko, python-jinja2, python-yaml, python-pkg-resources, python-crypto (>= 2.6), python-httplib2
Recommends: python-selinux
Suggests: ansible-doc, sshpass
Filename: pool/universe/a/ansible/ansible_1.7.2+dfsg-1~ubuntu14.04.1_all.deb
Size: 525392
MD5sum: 978cb184c196f2bbd83c88a57d23f432
SHA1: 03cf822df8fa27e1007f6b07be0aa8c042061c2c
SHA256: 6fc26e18a2ea13ac870d238dd255fe31fa59b46e085c3e832b161b23700479ea
Homepage: http://ansible.com
Description-en: Configuration management, deployment, and task execution system
 Ansible is a radically simple model-driven configuration management,
 multi-node deployment, and remote task execution system. Ansible works
 over SSH and does not require any software or daemons to be installed
 on remote nodes. Extension modules can be written in any language and
 are transferred to managed machines automatically.
Description-md5: db2b21b0d2286fccc4401d5982361488
```

## Show details about installed software

```apt-cache policy ansible```

```shell
eployuser@test-srv-1:~$ apt-cache policy ansible
ansible:
  Installed: (none)
  Candidate: 1.5.4+dfsg-1
  Version table:
     1.7.2+dfsg-1~ubuntu14.04.1 0
        100 http://mirrors.digitalocean.com/ubuntu/ trusty-backports/universe amd64 Packages
     1.5.4+dfsg-1 0
        500 http://mirrors.digitalocean.com/ubuntu/ trusty/universe amd64 Packages
```

## Check dependencies for a particular package

```shell
apt-cache showpkg ansible
```

## How to update system pacakges

```apt-get update```

## How to update software packages

```ap-get upgrade```

## How to install packages without upgrading

```apt-get install ansible --no-upgrade```

## How to upgrade specific packages

```apt-get install ansible --only-upgrade```

## How to install a specific version of a software

```apt-get install ansible=2.4.3```

## Clean retrieved .db files (packages) from local repository

```apt-get clean```

## Download only source code of the package

```apt-get --download-only source ansible```

## Download, unpack and compile a package

```apt-get --compile source ansible```

## Download a packge without installing

```apt-get download ansible```

## How do i check an application for broken dependencies

```apt-get check```

## Search and build dependencies


```apt-get build-dep ansible```

## Remove package and it's dependencies

```apt-get autoremove ansible```

## View packe installation history

```less /var/log/apt/history.log```

## Remove package without removing configuration files

Removes software package without removing configuration files, configuration files can be re-used.

```apt-get remove ansible```

## remove software package completely

This will remove software and all configuration files

```shell
sudo apt-get purge <PACKAGENAME>
sudo apt-get purge $(apt-cache depends <PACKAGENAME> | awk '{ print $2 }' | tr '\n' ' ')
sudo apt-get autoremove
sudo apt-get update
sudo apt-get check
sudo apt-get -f install
sudo apt-get autoclean
```
