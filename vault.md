# Vault

## Creating your vault

- create configuration file:
```mkdir /etc/vault.d/data``` the data directory is used to store your vault data files

```bash
storage "raft" {
  path    = "/etc/vault.d/data"
  node_id = "node1"
}

listener "tcp" {
  address     = "0.0.0.0:8200"
  tls_disable = "true"
}

api_addr = "http://127.0.0.1:8200"
cluster_addr = "https://0.0.0.1:8201"
ui = true  # disable by default
```

## Start vault

```vault server --config=<path-to-your-vault-configuration-file>``` in this case ```vault server --config=/etc/vault.d/config.hcl &```

## Check the status of vault

You will need to initialize the vault variable ```VAULT_ADDR``` (since we disable tls within the confguration file) ```export VAULT_ADDR=http://127.0.0.1:8200```

```vault status```

## Initialize vault

```vault operator init``` This will create you keys needed to unseal vault and the token to log into vault, by default vault will create 5 unseal keys with a threshold of 3 to unsearl vault

```vault
Unseal Key 1: T8tsu6iyz2IVRQfGVkFBIZ6BG1JmE61c7aWKVGV9UWu3
Unseal Key 2: dqPFlQVca4YUTJfbVn8SmTix6IFWmUpNAA4/D1Pa1lQe
Unseal Key 3: RT0PPAdk0DOk1U/uHaZ7AqNqzQqLaiZfnsjweALsdpOk
Unseal Key 5: c84h5TUP3UnUn37dxcYKwSFiNndQssO8G6FSfr5IQDyi

Initial Root Token: s.fA5lXz506qYJkvi6n41yFRl9
```

## Unseal vault

To unseal vault you can either use the cli command ```vault operator unseal``` or log into the web UI using the URL you defined with the vault configuration file ```address = "0.0.0.0:8200"``` usng 0.0.0.0 means you can enter any ip address you server is configured within an browser for example ```http://10.0.0.164``` and provide 3 of the 5 keys displayed from the vault initialize procedure. YOu will have to enter the ```vault operator unseal```command based on the key threshold in this case it's three.