# Git

## Set up your configuration

- ```git config --global -e```                                                - open your git configuration file.
- ```git config --global user.name "Clarence Mills"```                        - Set your name
- ```git config --global user.email clarence.mills@gmail.com```               - Set you email address
- ```git config --global core.editor "code --wait"```                         - Set your code editor to use visual studio code, the wait command waits for you to close the editor after use
- ```git config --global user.signingkey <gpg-key-id>```                      - Set you signing key
- ```git config --global core.excludesfile ~/.gitignore_global```             - Set you global exclude file
- ```git config --global help.autocorrect 1```                               - Turn on autocorrect with a 1 second delay

when you use any of the above commands a .gitconfig file will be created within your ~/.gitconfig home directory.

## archive

```git archive dev | gzip > tmp/latest.tgz```

- command is run from within the repository on your local server/workstation

## Remove branches

Removing branches  use the ```branch``` option from the git command using either ```--delete``` or ```-D```

### Sync local and remote branch

This will sync branches from the remote repository, removing branches that have been removed from the remote repository

```git pull --prune```

### Delete local branches

```git branch --delete <branch-name>```

### Delete remote branches

```git push origin --delete <branch-name>```

### copy a file from another branch

- checkout the branch that you want to copy the file or folder to ```git checkout -b <branch-name>```
- Copy the file from the other branch to the branch you are on ```git checkout <other_branch> -- <filename or folder>
