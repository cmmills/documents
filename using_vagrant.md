# Vagrant

## spin up container

Get list of containers from [Vagrant os options](http://www.vagrantbox.es/)

```bash
 vagrant box add {title} {url}
 vagrant init {title}
 vagrant up
 ```


https://bitbucket.org/cmmills/vagrant/src/master/centos7/Vagrantfile