# Systemctl

## List failed services

```systemctl list-units --state=failed```

## Clear failed services

Use systemctl to remove the failed status. To reset all units with failed status:

```systemctl reset-failed```

links

- [Digital ocean - docs](https://www.digitalocean.com/community/tutorials/how-to-use-systemctl-to-manage-systemd-services-and-units)