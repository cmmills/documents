# Code Server

## Installation

Download a link from Coder server [releases](https://github.com/cdr/code-server/releases)

```wet https://github.com/cdr/code-server/releases/download/3.2.0/code-server-3.2.0-linux-x86_64.tar.gz```

untar the downloaded application.

```tar -xzvf https://github.com/cdr/code-server/releases/download/3.2.0/code-server-3.2.0-linux-x86_64.tar.gz```

Move / copy the folder to /usr/bin/. This will allow you to execute the application easily.

## Starting the application

```/usr/bin/code-server/code-server --bind-addr 0.0.0.0:8010 --auth none --user-data-dir /home/oss/Documents```

The above command will start the application by binding to all interfaces ```0.0.0.:8080``` on port 8080 and not require any authentication.