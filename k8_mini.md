# Kubernetes environments

## Minikube
- [My Docs](minikube.md)

## Kind

### Links



- [https://minikube.sigs.k8s.io/docs/handbook/controls/](minikube)
- [https://kind.sigs.k8s.io/docs/user/quick-start/](kind)
- [https://docs.kubelinter.io/#/using-kubelinter](kubelinter)
- [https://www.katacoda.com/courses/kubernetes](OnlineTraining)
