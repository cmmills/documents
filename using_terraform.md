# Using Terraform

**Bug:** If using awscli config file Terraform will not be able to read the region from the file ~/.aws/configure

## Building manually

## Instance

  - Network                             - VPC
  - Subnet                              - default subnet
  - Auto-assign                         -  Use subnet settings
  - Security Group                      - Create new security group that allows port 22
  - Create key pair.                    - create a new key pair

## Enabling Internet Access

To enable access to or from the Internet for instances in a VPC subnet, you must do the following:

- Attach an Internet gateway to your VPC.
- Ensure that your subnet's route table points to the Internet gateway.

- Ensure that instances in your subnet have a globally unique IP address (public IPv4 address, Elastic IPaddress, or IPv6 address)

- Ensure that your network access control and security group rules allow the relevant traffic to flow to and from your instance.

Tip Due to a bug with Terraform, you will still need to specify region = us-east-1 (or your region) in your Terraform configurations.
This is because Terraform does not seem to read the config file in some circumstances.

## Graph terraform state

```terraform graph | dot -Tpng > graph.png```

Creating an ec2 instance
```
resource "aws_instance" "instance-name"{
  ami             = ami-13e45c77" - instance name
  instance_type   = "t2.micro"
  security_groups = ["${aws_security_group.web-sg-1.name}"]     # this will look at all *.tf files looking for a security group name "aws_security_group"
```

### field and map this instance to that security group 

```
  security_groups = ["web-sg"]  
  tags (
    Name  = "assign-a-name"
  )
}
```

### AWS instance gains access to the internet

- instance (two ip, from aws_subnet) --> aws_subnet(map_public_ip_on_launch = true) --> default route --> aws_route_table_association --> aws_route_table --->  aws_internet_gateway --> aws_security group

```
provisioner "local-exec" {
  command = "cat <<EOF > hosts
[dev]
${aws_instance.www-1.public_ip}
EOF"
}
```