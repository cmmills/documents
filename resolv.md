# Resolv

## Control

```systemctl start/stop/status/restart systemd-resolved```

 ### Configuration file

 ```/etc/systemd/resolved.conf```
 ```shell
 #  This file is part of systemd.
#
#  systemd is free software; you can redistribute it and/or modify it
#  under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation; either version 2.1 of the License, or
#  (at your option) any later version.
#
# Entries in this file show the compile time defaults.
# You can change settings by editing this file.
# Defaults can be restored by simply deleting this file.
#
# See resolved.conf(5) for details

[Resolve]
#DNS=
#FallbackDNS=
#Domains=
#LLMNR=no
#MulticastDNS=no
#DNSSEC=no
#DNSOverTLS=no
#Cache=yes
#DNSStubListener=yes
#ReadEtcHosts=yes
```

## Troubleshooting resolver

resolvectl

- Get status ```resolvectl status```

Other commands:

```shell
resolvectl [OPTIONS...] COMMAND ...

Send control commands to the network name resolution manager, or
resolve domain names, IPv4 and IPv6 addresses, DNS records, and services.

Commands:
  query HOSTNAME|ADDRESS...    Resolve domain names, IPv4 and IPv6 addresses
  service [[NAME] TYPE] DOMAIN Resolve service (SRV)
  openpgp EMAIL@DOMAIN...      Query OpenPGP public key
  tlsa DOMAIN[:PORT]...        Query TLS public key
  status [LINK...]             Show link and server status
  statistics                   Show resolver statistics
  reset-statistics             Reset resolver statistics
  flush-caches                 Flush all local DNS caches
  reset-server-features        Forget learnt DNS server feature levels
  dns [LINK [SERVER...]]       Get/set per-interface DNS server address
  domain [LINK [DOMAIN...]]    Get/set per-interface search domain
  default-route [LINK [BOOL]]  Get/set per-interface default route flag
  llmnr [LINK [MODE]]          Get/set per-interface LLMNR mode
  mdns [LINK [MODE]]           Get/set per-interface MulticastDNS mode
  dnsovertls [LINK [MODE]]     Get/set per-interface DNS-over-TLS mode
  dnssec [LINK [MODE]]         Get/set per-interface DNSSEC mode
  nta [LINK [DOMAIN...]]       Get/set per-interface DNSSEC NTA
  revert LINK                  Revert per-interface configuration

Options:
  -h --help                    Show this help
     --version                 Show package version
     --no-pager                Do not pipe output into a pager
  -4                           Resolve IPv4 addresses
  -6                           Resolve IPv6 addresses
  -i --interface=INTERFACE     Look on interface
  -p --protocol=PROTO|help     Look via protocol
  -t --type=TYPE|help          Query RR with DNS type
  -c --class=CLASS|help        Query RR with DNS class
     --service-address=BOOL    Resolve address for services (default: yes)
     --service-txt=BOOL        Resolve TXT records for services (default: yes)
     --cname=BOOL              Follow CNAME redirects (default: yes)
     --search=BOOL             Use search domains for single-label names
                                                              (default: yes)
     --raw[=payload|packet]    Dump the answer as binary data
     --legend=BOOL             Print headers and additional info (default: yes)

See the resolvectl(1) man page for details.
```