# Command line shortcuts

## Go to the beginning of a line and add a word

```ctrl+a```

an alternative to the above would be ```ctrl+u``` to remove everyting to the beginning of the line, type your word you left out then ```ctrl+y``` to bring back what you erased.

## Go to the end of a line

```ctrl+e```

## delete everything after to the right of the cursor to the end of the line

```ctrl+k```

e.g ```find / -name wings | xargs ls -la```
after ```find / -name wings```

## Delte everything to the left of the curusor to the beginning of the line

```ctrl+u```

## To reverse both commands above

```ctrl+y```

## To remove one word at a timee reverse

```ctrl+w```

## replace tail  with less

replace ```tail -f <filename>``` with ```less +F <filename>``` starts at the end of the file
```ctrl+c``` allows you to move forwards and backwards on the file.
```shift+F``` puts you back into follow mode and the end of the file.
```q``` to quit.

## writing shell script on the command line and you run of space

```ctrl+x+e``` will place you automatincally into memory buffer with whatever is set to  an editor in your shell.
use ```$(EDITOR)``` to find out the editor while you are in the buffer

## replacing commands

suppose you are running a ping commmand for example ping 8.8.8.8 and you want to use the 8.8.8.8 with another command use ```alt .```` (alt period)

example:

```bash
 :> ping www.opensitesolutions.com
PING www.opensitesolutions.com (192.155.88.243) 56(84) bytes of data.
64 bytes from li572-243.members.linode.com (192.155.88.243): icmp_seq=1 ttl=52 time=54.6 ms
64 bytes from li572-243.members.linode.com (192.155.88.243): icmp_seq=2 ttl=52 time=45.9 ms
64 bytes from li572-243.members.linode.com (192.155.88.243): icmp_seq=3 ttl=52 time=52.1 ms
64 bytes from li572-243.members.linode.com (192.155.88.243): icmp_seq=4 ttl=52 time=46.8 ms
64 bytes from li572-243.members.linode.com (192.155.88.243): icmp_seq=5 ttl=52 time=42.7 ms
^C
--- www.opensitesolutions.com ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4006ms
rtt min/avg/max/mdev = 42.698/48.415/54.630/4.342 ms
cmills@HP-ENVY: 2021-04-25 ~/Documents/docs/applicaitions (master)
 M commmand_line_shortcuts.md
 :> mtr www.opensitesolutions.com

 HP-ENVY (10.0.0.124)                                                                                                                                                                                        2021-04-25T20:52:05-0400
Keys:  Help   Display mode   Restart statistics   Order of fields   quit
                                                                                                                                                                                            Packets             Pings
 Host                                                                                                                                                                                     Loss%   Snt   Last   Avg  Best  Wrst StDev
 1. Docsis-Gateway.phub.net.cable.rogers.com                                                                                                                                               0.0%    16   13.7  23.3   6.9 137.1  32.1
 2. 99.224.112.1                                                                                                                                                                           6.2%    16   35.5  28.2  17.9  66.9  12.6
 3. 24.156.151.225                                                                                                                                                                         6.2%    16  126.6  38.3  18.5 126.6  28.8
 4. 3009-cgw01.wlfdle.rmgt.net.rogers.com                                                                                                                                                  6.2%    16   89.5  50.8  19.6 165.4  47.2
 5. 209.148.233.102                                                                                                                                                                        0.0%    16   35.2  83.6  32.4 262.3  70.3
 6. if-be-45.ecore1.aeq-ashburn.as6453.net                                                                                                                                                 0.0%    16   63.2  66.0  35.7 209.2  48.4
 7. if-ae-55-2.tcore3.aeq-ashburn.as6453.net                                                                                                                                               0.0%    16   63.2  79.2  55.2 217.4  41.4
 8. if-ae-12-2.tcore4.njy-newark.as6453.net                                                                                                                                                0.0%    16   52.4  82.1  43.4 178.3  46.2
 9. 66.198.111.166                                                                                                                                                                         0.0%    16   59.6  76.0  58.4 196.6  33.2
10. 173.255.239.11                                                                                                                                                                         0.0%    15  174.5  86.8  58.4 174.5  39.6
11. li572-243.members.linode.com                                                                                                                                                           0.0%    15  121.6  61.5  40.2 121.6  25.9
```

```ctrl+c``` the last command type the new command in this case mtr and tyhpe ```alt .``` ```mtr www.opensiteolsutions.com```, ```atl .``` will subsititue the last parameter to mtr command

## if you screw up you terminal

use the ```reset``` command instead of killing and resarting the terminal.2

