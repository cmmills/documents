# Log rotate

The default configuration file is ```/etc/logrotate.conf``` and service or server specific configurations are stored in ```/etc/logrotate.d``` directory.

Check what will happen when logrotate is forced (no changes will be made):

```$ logrotate -vdf CONFIG_FILE```

Manually force logrotate:

```$ logrotate -vf CONFIG_FILE```

## links

[shellhacks](https://www.shellhacks.com/logrotate-force-log-rotation/)
