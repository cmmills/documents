# DNSDIST

Reverse proxy load balancer

- DNS delivery controller
- DNS shaping
- DNS monitoring
- DOS mitigation
- Receives DNS query packets from clients
- Forwards these to a backend (downstream) server
- Forwards the response packet to the client

Downloads: [Repo](https://repo.powerdns.com/)

## Commands

Get dnsdist version

```shell
pi@k8wk-2:~ $ dnsdist -V
dnsdist 1.3.3 (Lua 5.2.4)
```

## Configuration

### Check configuration

```shell
pi@k8wk-2:~ $ dnsdist --check-config
Configuration '/etc/dnsdist/dnsdist.conf' OK!
```

### Access control list
 
- ```addACL('192.168.0.1/24')``` This will add the network to the default RFC1918 network list
- ```setACL('192.168.0.2/24')``` this will remove all th RFC1918 networks leaving only this network in place
- ```addACL('0.0.0.0/0')``` This will add Allow connections from all networks not removing the RFC1918 networks
- ```setACL('0.0.0.0/0')``` Allow allow network removing RFC1918 network from the list

### Policies

- firstAvailable: Pick firs server that has not exceeded it's QPS limit, ordered by the server 'order' parameter
- wrandom: Weighted ramdon over available servers, based on the server 'weight' parameter.
- roundrobin: Simple round robin over available servers
- leastOutstanding: Send traffic to downstream oserver with least outstanding queries, with the lowest 'order', and within that the lowest recent latency


## configuration file

- ```/etc/dnsdist/dnsdist.conf``` on raspberry pi

[config](configs/dnsdist.conf)

## Settings the control socket

- update the dnsdist configuratin file - /etc/dnsdist/dnsdist.conf, add ```controlSocket('192.0.2.53:5199')```. This will bind to all interfaces.
- use the command ```dnsdist -l 127.0.0.1:5300 to bind to the controlsocket to be able to generate a encoded key.

```shell
> makeKey()
setKey("8V6/V+AGrRyZUKUWvAhd728XV4Rr/jZPYeVft7+9sl4=")
```

- add the following section into your dnsdist.conf file
```setKey("8V6/V+AGrRyZUKUWvAhd728XV4Rr/jZPYeVft7+9sl4=")```
- check your ocnfiguration before restarting

```shell
pi@k8wk-2:~ $ dnsdist --check-config
Configuration '/etc/dnsdist/dnsdist.conf' OK!
```

## connect to control socket

Once your dnsdist servers is configured from above you should be able to connect to the controlsocket by issuing the folllowign commnd:

- Connect locally ```dnsdist -c ```

### Links

- [dndist explained](https://berthub.eu/tmp/dnsdist-md/dnsdist-diagrams.md.html)
- [Dnsdist explained](https://blog.apnic.net/2020/02/28/how-to-deploy-dot-and-doh-with-dnsdist/)
- [Good examples](https://berthub.eu/tmp/dnsdist-md/dnsdist-diagrams.md.html)
- [DOT and DOH dnsdist](https://dnsprivacy.org/wiki/display/DP/Using+dnsdist+for+DoT+and+DoH)