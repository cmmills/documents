# Recursor

Read

- [performance guide](https://docs.powerdns.com/recursor/performance.html)

## Configuration

/etc/powerdns/recursor (ubuntu bionic)

## Custom configuration file

[recursor.conf](configs/recursor.conf)

### Command line

Get version

```shell
pdns_recursor --help
```

### Webserver

[API-key](https://docs.powerdns.com/recursor/http-api/index.html)

Resolving name server that runs as a single threaded seperate process . Recursive lookup and caching server, it finds answer for DNS queries from clients.

Recursor = resolver

- [Ansible role provided by Powerdns](https://github.com/PowerDNS/pdns_recursor-ansible/)

example  recursive configuration file

### Forward zones

Resolve your own zones internally, don't check on the internet.

[Powerdns forward-zones](https://doc.powerdns.com/recursor/settings.html#forward-zones)

```shell
forward-zones=example.org=203.0.113.210, powerdns.com=2001:DB8::BEEF:5  
# etc..
```

other examples:

- [forward-zone examples from POwerdns](https://doc.powerdns.com/authoritative/guides/recursion.html#scenario-2-authoritative-server-as-recursor-for-clients-and-serving-public-domains)
- [Recursor zone](https://docs.powerdns.com/recursor/http-api/zone.html)
- [Hulu setup](https://medium.com/hulu-tech-blog/dns-infrastructure-at-hulu-c69fd20170dc)

### Forward zones file

```shell
internal-zone.com=10.65.10.40;10.65.10.41
+.=8.8.8.8,1.1.1.1
```

## Links

- [Getting Started](https://doc.powerdns.com/recursor/getting-started.html)
- Example configuration
  - [1](https://www.thatfleminggent.com/getting-a-powerdns-recursor-up-and-going-fast/)
  - [2](https://itsmetommy.com/2018/12/13/powerdns-recursor/)
  - [Troubleshooting](https://doc.powerdns.com/recursor/running.html)