# Power DNS

## Authoratative Server

Either a primary or secondary

Server that holds the Zone file within either a flat file or Database server

### Install

Ubuntu:

[Power DNS Repo](https://repo.powerdns.com)

Create the file '/etc/apt/sources.list.d/pdns.list' with this content:

```deb [arch=amd64] http://repo.powerdns.com/ubuntu bionic-auth-43 main```

And this to '/etc/apt/preferences.d/pdns':

```shell
Package: pdns-*
Pin: origin repo.powerdns.com
Pin-Priority: 600
```

and execute the following commands:

```shell
curl https://repo.powerdns.com/FD380FBB-pub.asc | sudo apt-key add -
sudo apt-get update
sudo apt-get install -y pdns-server
sudo apt-get install -y pdns-backend-mysql
```

### Set up database

- log into mysql database
  - ```create database pdns;```
  - Create user
    - ```GRANT ALL PRIVILEGES ON *.* TO 'powerdns'@localhost IDENTIFIED BY 'gr33n';``` Give all prvileges to powerdns user on all items within mysql domain
    - ```GRANT ALL PRIVILEGES ON 'pdns'.* TO 'powerdns'@localhost;``` Give access only to particular database;
    - ```FLUSH PRIVILEGES;``
- copy mysql [configuration]https://github.com/PowerDNS/pdns/blob/rel/auth-4.1.x/modules/gmysqlbackend/schema.mysql.sql) from powerdns website into a file(pdns_db.mysql) and import into the database created from the step prior
  - ```mysql -u root pdns < pdns_db.mysql```

- Update powerdns configuration to access database

  ```shell
    launch=gmysql
    gmysql-host=127.0.0.1
    gmysql-user=powerdns
    gmysql-dbname=pdns
    gmysql-password=<secure-password>
   ```

### Authoratative settings

```master=yes``` change from no

### Resolving

By default the server can resolve names it's authoratative for and will query internet registrars for names unknown. To use and internal resolver like a recursor update the configuration ```resolver=192.168.0.22```

### Misc

- operate as a daemon ```daemon=yes```

#### Create a new zone 

```pdnsutil create-zone millsresidence.com ns1.millsresidence.com```

e.g

```shell
i@compute-2:~ $ sudo pdnsutil create-zone millsresidence.com ns1.millsresidence.com
Jul 04 08:03:34 [bindbackend] Done parsing domains, 0 rejected, 0 new, 0 removed
Creating empty zone 'millsresidence.com'
Also adding one NS record
```

#### Create and A record

- create name server record
```pdnsutil add-record millsresidence.com ns1 A 192.168.0.18```

- change zone to to master.

```shell
pi@compute-2:/etc/powerdns $ sudo pdnsutil set-kind millsresidence.com master
Jul 04 08:20:38 [bindbackend] Done parsing domains, 0 rejected, 0 new, 0 removed

pi@compute-2:/etc/powerdns $ sudo pdnsutil show-zone millsresidence.com
Jul 04 08:21:12 [bindbackend] Done parsing domains, 0 rejected, 0 new, 0 removed
This is a Master zone
Last SOA serial number we notified: 1 == 1 (serial in the database)
Zone is not actively secured
Metadata items: None
No keys for zone 'millsresidence.com'.
```

### Ignore /etc/resolv.conf

```resolver```
IP Addresses with optional port, separated by commas
New in version 4.1.0.

Use these resolver addresses for ALIAS and the internal stub resolver. If this is not set, /etc/resolv.conf is parsed for upstream resolvers.

### links

- DNSDIST
  - [dnsdist](dnsdist.md)

- Recurosr
  - [recursor](recursor.md)

- [Authoratative](https://doc.powerdns.com/authoritative/)
  - [Howto](https://doc.powerdns.com/authoritative/guides/index.html)
    - [Migrating to PowerDNS](https://doc.powerdns.com/authoritative/migration.html)
  - [Modes of operation](https://doc.powerdns.com/authoritative/modes-of-operation.html)
- Example setups
  - [Admin-magazine](https://www.admin-magazine.com/Articles/Speed-up-Your-Name-Server-with-a-MySQL-Back-End)
  - [Scaleway - mysql repliction](https://www.scaleway.com/en/docs/installing-powerdns-server-on-ubuntu-bionic/)
   - [Create zone](https://blog.powerdns.com/2016/02/02/powerdns-authoritative-the-new-old-way-to-manage-domains/)

- Web Frontend
  - [Powerdns-Admin](https://github.com/ngoduykhanh/PowerDNS-Admin)
    - [Wiki](https://github.com/ngoduykhanh/PowerDNS-Admin/wiki)