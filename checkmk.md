# CheckMK

## Register agent with Checmk server

```cmk-agent-ctl register --hostname checkmk --server  --site millsresidence --user automation --password < automation_user_password >```


## Force agent sync

```bash
systemctl enable --now check-mk-agent-async.service
systemctl enable --now check-mk-agent.socket
systemctl enable --now cmk-agent-ctl-daemon.service
systemctl enable --now cmk-update-agent.timer
```