# logical volumes

```bash
pvcreate /dev/sdb   - this creates 

vgcreate data /dev/sdb - 

lvcreate -l 100%FREE -n data_vol data             - create logical volume group

mkfs.xfs /dev/data/data_vol                       - Format volumet to xfs

mkdir /data

mount /dev/data/data_vol /data
```

Check for disk that are available using the ```lsblk``` command , in this case **sdb** is availalble
using
```bash
[root@srv-1 ~]# lsblk
NAME            MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
sda               8:0    0 223.6G  0 disk 
├─sda1            8:1    0     1G  0 part /boot
└─sda2            8:2    0 222.6G  0 part 
  ├─centos-root 253:0    0    50G  0 lvm  /
  ├─centos-swap 253:1    0   7.8G  0 lvm  [SWAP]
  └─centos-home 253:2    0 164.8G  0 lvm  /home
sdb           
    8:16   1 235.8G  0 disk
```

Before modifying the disk, confirm there are no partitions (partitions are a good indication there's data on the disk).

```bash
ss@srv-1 ~]$ lsblk
NAME                   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
sda                      8:0    0 223.6G  0 disk 
├─sda1                   8:1    0     1G  0 part /boot
└─sda2                   8:2    0 222.6G  0 part 
  ├─centos-root        253:0    0    50G  0 lvm  /
  ├─centos-swap        253:1    0   7.8G  0 lvm  [SWAP]
  └─centos-home        253:2    0 164.8G  0 lvm  /home
sdb                      8:16   1 235.8G  0 disk 
└─ext_storage-data_vol 253:3    0 235.8G  0 lvm  
sdc                      8:32   1  59.6G  0 disk 
├─sdc1                   8:33   1   2.3G  0 part 
├─sdc2                   8:34   1   3.9M  0 part 
└─sdc3                   8:35   1  57.3G  0 part 
```


If there are, you should delete the partion(s) before starting.

```bash
[root@srv-1 ~]# fdisk /dev/sdb
Welcome to fdisk (util-linux 2.23.2).

Changes will remain in memory only, until you decide to write them.
Be careful before using the write command.


Command (m for help): m       
Command action
   a   toggle a bootable flag
   b   edit bsd disklabel
   c   toggle the dos compatibility flag
   d   delete a partition
   g   create a new empty GPT partition table
   G   create an IRIX (SGI) partition table
   l   list known partition types
   m   print this menu
   n   add a new partition
   o   create a new empty DOS partition table
   p   print the partition table
   q   quit without saving changes
   s   create a new empty Sun disklabel
   t   change a partition's system id
   u   change display/entry units
   v   verify the partition table
   w   write table to disk and exit
   x   extra functionality (experts only)

Command (m for help): d
Selected partition 1
Partition 1 is deleted

Command (m for help): w
The partition table has been altered!

Calling ioctl() to re-read partition table.
Syncing disks.
```

Create a new physical volume on /dev/sdb.

```bash
[root@srv-1 ~]# pvcreate /dev/sdb
WARNING: dos signature detected on /dev/sdb at offset 510. Wipe it? [y/n]: y
  Wiping dos signature on /dev/sdb.
  Physical volume "/dev/sdb" successfully created.
```

Verify the newly created physical volume using the ```pvs``` command.

```bash
[root@srv-1 ~]# pvs
  PV         VG     Fmt  Attr PSize    PFree  
  /dev/sda2  centos lvm2 a--  <222.57g   4.00m
  /dev/sdb          lvm2 ---   235.75g 235.75g
```

or using the ```pvdisplay``` command

```bash
[oss@srv-1 ~]$ sudo pvdisplay
  "/dev/sdc" is a new physical volume of "59.62 GiB"
  --- NEW Physical volume ---
  PV Name               /dev/sdc
  VG Name               
  PV Size               59.62 GiB
  Allocatable           NO
  PE Size               0   
  Total PE              0
  Free PE               0
  Allocated PE          0
  PV UUID               a9ORet-4sx6-Xpbd-Ul44-58Wo-z1Tf-m0mQpF
```

Create a volume group

```bash
[root@srv-1 ~]# vgcreate ext_storage /dev/sdb
  Volume group "ext_storage" successfully created
```

Verify the volume has been created.

```bash
[root@srv-1 ~]# pvs
  PV         VG          Fmt  Attr PSize    PFree   
  /dev/sda2  centos      lvm2 a--  <222.57g    4.00m
  /dev/sdb   ext_storage lvm2 a--  <235.75g <235.75g
```

or

```bash
[oss@srv-1 ~]$ sudo pvdisplay
  --- Physical volume ---
  PV Name               /dev/sdb
  VG Name               ext_storage         <--- new volume name
  PV Size               235.75 GiB / not usable 4.00 MiB
  Allocatable           yes (but full)
  PE Size               4.00 MiB
  Total PE              60351
  Free PE               0
  Allocated PE          60351
  PV UUID               vBMr3S-9dVc-zyZi-bCGy-OC17-H2Rb-10SN08
```

Create a logical volume that uses 100 percent of the disk.

```bash
[root@srv-1 ~]# lvcreate -l 100%FREE -n data_vol ext_storage                                                                                                                                                    
Logical volume "data_vol" created.
```

Create the filesystem on the newly created logical volume

```bash
[root@srv-1 ~]# mkfs.ext4 /dev/ext_storage/data_vol 
mke2fs 1.42.9 (28-Dec-2013)
Filesystem label=
OS type: Linux
Block size=4096 (log=2)
Fragment size=4096 (log=2)
Stride=0 blocks, Stripe width=0 blocks
15450112 inodes, 61799424 blocks
3089971 blocks (5.00%) reserved for the super user
First data block=0
Maximum filesystem blocks=2210398208
1886 block groups
32768 blocks per group, 32768 fragments per group
8192 inodes per group
Superblock backups stored on blocks: 
        32768, 98304, 163840, 229376, 294912, 819200, 884736, 1605632, 2654208, 
        4096000, 7962624, 11239424, 20480000, 23887872

Allocating group tables: done                            
Writing inode tables: done                            
Creating journal (32768 blocks): done
Writing superblocks and filesystem accounting information: done     
```

If you want to create a dos based filesystem you will need to install ```dosfstools```

- On Ubuntu/Debian, you can install dosfstools using the following command:

```sudo apt install dosfstools -y```

- On CentOS 7 or RHEL 7, you can install dosfstools with the following command:

```sudo yum install dosfstools -y```

command to create a dos based filesystem example: ```sudo mkfs.vfat -F 32 -n USBDRIVE /dev/usb_storage/data_vol```

Create a mount point to attach the newly created filesystem from above.

```bash
[root@srv-1 ~]# mkdir /ext_storage
```

Mount the file system partiton to the new mount point (ext_storage)

```bash
[root@srv-1 ~]# mount /dev/ext_storage/data_vol /ext_storage/
```

```bash
[root@srv-1 ~]# df -h
Filesystem                        Size  Used Avail Use% Mounted on
devtmpfs                          7.7G     0  7.7G   0% /dev
tmpfs                             7.8G     0  7.8G   0% /dev/shm
tmpfs                             7.8G   18M  7.7G   1% /run
tmpfs                             7.8G     0  7.8G   0% /sys/fs/cgroup
/dev/mapper/centos-root            50G  8.4G   42G  17% /
/dev/sda1                        1014M  198M  817M  20% /boot
/dev/mapper/centos-home           165G  767M  164G   1% /home
overlay                            50G  8.4G   42G  17% /var/lib/docker/overlay2/810b030eb3693967cf1eff93412681bbe4b7d7fc20b726d5f04bf7ab8f978114/merged
overlay                            50G  8.4G   42G  17% /var/lib/docker/overlay2/a6db98f7a2f1caae25430540378343eab2af6ae1b9050d185f659a8ce5aa4463/merged
tmpfs                             1.6G     0  1.6G   0% /run/user/1000
overlay                            50G  8.4G   42G  17% /var/lib/docker/overlay2/a54a1329486986c212e0816291b33dd210404b83cc21739827dbe7dec57e3aab/merged
/dev/mapper/ext_storage-data_vol  232G   61M  221G   1% /ext_storage
```

```bash
[root@srv-1 ~]# lsblk
NAME                   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
sda                      8:0    0 223.6G  0 disk 
├─sda1                   8:1    0     1G  0 part /boot
└─sda2                   8:2    0 222.6G  0 part 
  ├─centos-root        253:0    0    50G  0 lvm  /
  ├─centos-swap        253:1    0   7.8G  0 lvm  [SWAP]
  └─centos-home        253:2    0 164.8G  0 lvm  /home
sdb                      8:16   1 235.8G  0 disk 
└─ext_storage-data_vol 253:3    0 235.8G  0 lvm  /ext_storage
```



### Links

- [TecMint](https://www.tecmint.com/add-new-disks-using-lvm-to-linux/)
- [Digital ocean logical volumes](https://www.digitalocean.com/community/tutorials/an-introduction-to-lvm-concepts-terminology-and-operations)
- [How to create LVM in Linux](https://linoxide.com/linux-how-to/lvm-configuration-linux/)
- [Howto Forge](https://www.howtoforge.com/linux_lvm_p3)
- [logical Volumen management explained](https://devconnected.com/logical-volume-management-explained-on-linux/)
- [Digital ocean logical volumes](https://www.digitalocean.com/community/tutorials/an-introduction-to-lvm-concepts-terminology-and-operations)
- [How to create LVM in Linux](https://linoxide.com/linux-how-to/lvm-configuration-linux/)
- [Howto Forge](https://www.howtoforge.com/linux_lvm_p3)
- [logical Volumen management explained](https://devconnected.com/logical-volume-management-explained-on-linux/)