# Using TMUX

## Session management

```shell
tmux ls (or tmux list-sessions)
tmux new -s session-name
Ctrl-b d Detach from session
tmux attach -t [session name]
tmux kill-session -t session-name
Ctrl-b ? List all keybindings
```

## Window management

```shell
Ctrl-b c Create new window
Ctrl-b d Detach current client
Ctrl-b n (Move to the next window)
Ctrl-b p (Move to the previous window)
Ctrl-b l (Move to the previously selected window)
Ctrl-b w (List all windows / window numbers)
Ctrl-b & Kill the current window
Ctrl-b , Rename the current window
Ctrl-b q Show pane numbers (used to switch between panes)
Ctrl-b o Switch to the next pane
Ctrl-b window number (Move to the specified window number, the
```

## Tiling commands

```shell
Ctrl-b % (Split the window vertically)
CTRL-b " (Split window horizontally)"
Ctrl-b o (Goto next pane)
Ctrl-b q (Show pane numbers, when the numbers show up type the key to go to that pane)
Ctrl-b { (Move the current pane left)
Ctrl-b } (Move the current pane right)
Ctrl-b : "break-pane" Make a pane its own window
ctrl-b z Maximize and minimize a window
```

## add to ~/.tmux.conf

```shell
bind | split-window -h
bind - split-window -v
default bindings are from 0 -- 9)
```

## Random caommands

```shell
ctrl-b [ (enter q to quit) or ctrl-b PG up or PG dn - Scrolling
ctrl-b :setw synchronize-panes =- Synchronize winodws allows you to type in multiple windows. Enable and disable the command

```

## Get TMUX version information

```tmux -V```

### Links

[TMUX Wiki](https://github.com/tmux/tmux/wiki)
