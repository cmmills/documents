# Server configuration

## Set Mysql prompt

```\R MYSQL=(\u@\h) [\d] >```

- \u = will indicate the user that's currently logged in.
- \h = will show the host IP address or FQDN.
= \d = will show the database curently selected (none) will be displayed if no database is selected.