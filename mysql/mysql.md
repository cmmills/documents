# Mysql

## Create a database

```bash
create database finance
```

You can enter the above from the mysql command line or enter within a file and execute via the command line using the following 

```bash
source <path-to-filename>
```

e.g

```bash
source /home/cmills/Documents/db.sql
```

### use a database

```use <database_name>```

e.g

```use finance```

### Check which database you are currently using

```select database();```

## Find the size of a database

```mysql
mysql> SELECT ENGINE, COUNT(*), SUM(DATA_LENGTH), SUM(INDEX_LENGTH) FROM information_schema.TABLES WHERE TABLE_SCHEMA='finance' GROUP BY ENGINE;
+--------+----------+------------------+-------------------+
| ENGINE | COUNT(*) | SUM(DATA_LENGTH) | SUM(INDEX_LENGTH) |
+--------+----------+------------------+-------------------+
| InnoDB |        1 |            16384 |                 0 |
+--------+----------+------------------+-------------------+
1 row in set (0.02 sec)
```

### show the table within a database

```bash
show tables from <database-name>
```

### Show the table structure
```bash
show tables from <database-name>.<table-name>
```

## Mass Insert items into a table

Enable file import
```mysql -h 0.0.0.0 --local-infile=1 -uroot -pginger```

plus

```set global local_infile=1;```

then you will be able to import intems into the datbase.

```load data local infile '~/Desktop/links2.csv' into table web_links fields terminated by ','```

The above will load items from the file ```/home/cmills/Documents/links2.csv``` with columns seperated by commas.

## Create a table

```insert into stocks(stock_name,ticker_name,note) values('ambari','na','not listed as yet');```

## Create a table from a select statement

```create table subscription select * from bills;```

## Check table status

```bash
mysql> check table bills;
+---------------+-------+----------+----------+
| Table         | Op    | Msg_type | Msg_text |
+---------------+-------+----------+----------+
| finance.bills | check | status   | OK       |
+---------------+-------+----------+----------+
1 row in set (0.01 sec)
```

## Alter table

```alter table bills modify date int(2);``` 

## Update table row

```update bills set date = '21' where id = 21;```

or

```update bills set dollar_value=75.39 where company="some company name";```

## Insert values into a table

```insert into finance.bills (bank, date, company, currency, bill_usage, dollar_value, occurence, status) values ('mastercard', 3, 'AGI Geeksquad PRO', 'CA', 'Business', 28.24, 'monthly', 'Active');```

## Rename table

```rename table subscription to subscriptions;```

### Delete from a table

```delete from subscription where occurence != "yearly";```

## Change column name

```alter table bills rename column country to currency;```

## Add a new column to a table

```alter table finance.bills add column notes varchar(100) not null;```

## Change table entries case

```update bills set status=lower(status);```

## Search filters

### equal =

using the = will match exactly.

```select title,url from web_links where category = 'stocks';```

### like

using the like statement is an approximate match

```select title,url form web_links where category like'%item%';```

with and

```select title,url from web_links where category like '%stocks%' and link like '%tsn%';```

### not equal to

```select sum(dollar_value) from bills where bill_usage = 'entertainment' and status != 'canceled';```

## Shorten table column entries

```select company,dollar_value,concat(substring(notes, 1,10), '...')as shorten_notes from finance.bills;```

- substring(notes,1,10), shortend the contents within the column notes from the first character to the 10 character

## Dates

### Select date range

calculate dollar amount between dates.
```select sum(dollar_value) from finance.bills where date between 1 and 15 order by date and status = 'Active';```

Show bank,company,dollar value between dates that match bank and from 15 of the month to the 30 of the month.
```select bank,company,dollar_value,date from finance.bills where date between 15 and 30 and status = 'Active' and bank = 'simplii bank';```

### Calculate days from current date

```bash

mysql> select date_add(curdate(), interval 9 day) '9 days later';
+--------------+
| 9 days later |
+--------------+
| 2020-12-15   |
+--------------+
1 row in set (0.01 sec)
```

Curdate = 6, 9 days later would be the 15.

### Day of the week

```bash
mysql> select dayofweek(date_add(curdate(), interval 9 day));
+------------------------------------------------+
| dayofweek(date_add(curdate(), interval 9 day)) |
+------------------------------------------------+
|                                              3 |
+------------------------------------------------+
1 row in set (0.00 sec)
```

curdate = 6, 9 days from the 6 is 15, which is tuesday Dec 15,2020.


### Day of the month

```bash
mysql> select dayofmonth(date_add(curdate(), interval 9 day));
+-------------------------------------------------+
| dayofmonth(date_add(curdate(), interval 9 day)) |
+-------------------------------------------------+
|                                              15 |
+-------------------------------------------------+
1 row in set (0.00 sec)
```

## Get bills based on X days from current date

```bash
mysql> select * from finance.bills where day = (select dayofmonth(date_add(curdate(), interval 2 day))) and occurence = 'monthly';
+----+--------------+------+-------------------------+----------+---------------+--------------+-----------+----------+----------------+
| id | bank         | date | company                 | currency | bill_usage    | dollar_value | occurence | status   | notes          |
+----+--------------+------+-------------------------+----------+---------------+--------------+-----------+----------+----------------+
|  1 | mastercard   |   15 | some company name 1     | CA       | home          |        42.66 | monthly   | active   | Home insurance |
|  6 | mastercard   |   15 | some company name 2     | US       | entertainment |         2.99 | monthly   | canceled |                |
|  8 | simplii bank |   15 | some company name 3     | CA       | home          |       934.06 | monthly   | active   |                |
+----+--------------+------+-------------------------+----------+---------------+--------------+-----------+----------+----------------+
3 rows in set (0.00 sec)

```

### Get bills based on x days in the past

```bash
ysql> select * from bills where day = (select dayofmonth(date_add(curdate(), interval -3 day)));
+----+------------+------+-------------------+----------+------------+--------------+-----------+--------+-------+
| id | bank       | date | company           | currency | bill_usage | dollar_value | occurence | status | notes |
+----+------------+------+-------------------+----------+------------+--------------+-----------+--------+-------+
| 29 | mastercard |    3 | some company name | CA       | business   |        28.24 | monthly   | active |       |
+----+------------+------+-------------------+----------+------------+--------------+-----------+--------+-------+
1 row in set (0.01 sec)
```

### Links

- [TechontheNet](https://www.techonthenet.com/mysql/index.php)
- [Phonix Nap](https://phoenixnap.com/kb/category/databasesn)
- [The Geek Diary - Beginners guide to mysql user management](https://www.thegeekdiary.com/beginners-guide-to-mysql-user-management/)
- [Mysql Tutorial](https://www.mysqltutorial.org)
- [Tutorialpoint - fetch record to specify month and year](https://www.tutorialspoint.com/how-can-fetch-records-from-specific-month-and-year-in-a-mysql-table)
- [Understanding day and month](https://www.w3resource.com/mysql/date-and-time-functions/mysql-dayofmonth-function.php)
