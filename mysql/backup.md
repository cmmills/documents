# Percona 

## Installing Percona XtraBackup via percona-release
Percona XtraBackup, like many other Percona products, is installed via the percona-release package configuration tool.

Download a deb package for percona-release the repository packages from Percona web:

```bash
$ wget https://repo.percona.com/apt/percona-release_latest.$(lsb_release -sc)_all.deb
```

Install the downloaded package with dpkg. To do that, run the following commands as root or with sudo: ```dpkg -i percona-release_latest.$(lsb_release -sc)_all.deb```

Once you install this package the Percona repositories should be added. You can check the repository setup in the ```/etc/apt/sources.list.d/percona-release.list``` file.

Enable the repository: ```percona-release enable-only tools release```

If Percona XtraBackup is intented to be used in combination with the upstream MySQL Server, you only need to enable the tools repository: ```percona-release enable-only tools.```

Remember to update the local cache: ```apt-get update```

After that you can install the percona-xtrabackup-80 package:

```bash
$ sudo apt-get install percona-xtrabackup-80
```

In order to make compressed backups, install the qpress package:

```$ sudo apt-get install qpress```


## Create a backup

```xtrabackup --backup --target-dir=/home/cmills/mysql_backup --datadir=mysql_data --user=root --password=ginger --host=0.0.0.0```


### Links
- [Install percona backup](https://www.percona.com/doc/percona-xtrabackup/8.0/installation/apt_repo.html)
