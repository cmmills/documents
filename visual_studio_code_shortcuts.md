# Visual studio code shortcuts


## Editing Commands

```bash
Ctrl+X Cut line (empty selection)
Ctrl+C Copy line (empty selection)
Alt+ ↓ / ↑ Move line down/up
Ctrl+Shift+K Delete line
Ctrl+Enter /
Ctrl+Shift+Enter
Insert line below/ above
Ctrl+Shift+\ Jump to matching bracket
Ctrl+] / Ctrl+[ Indent/Outdent line
Home / End Go to beginning/end of line
Ctrl+ Home / End Go to beginning/end of file
Ctrl+ ↑ / ↓ Scroll line up/down
Alt+ PgUp / PgDn Scroll page up/down
Ctrl+Shift+ [ / ] Fold/unfold region
Ctrl+K Ctrl+ [ / ] Fold/unfold all subregions
Ctrl+K Ctrl+0 /
Ctrl+K Ctrl+J
Fold/Unfold all regions
Ctrl+K Ctrl+C Add line comment
Ctrl+K Ctrl+U Remove line comment
Ctrl+/ Comment out code
Ctrl+Shift+A Toggle block comment
```

## Multi-cursor and selection

```bash

Alt+Click Insert cursor*
Shift+Alt+ ↑ / ↓ Insert cursor above/below
Ctrl+U Undo last cursor operation
Shift+Alt+I Insert cursor at end of each line selected
Ctrl+L Select current line
Ctrl+Shift+L Select all occurrences of current selection
Ctrl+F2 Select all occurrences of current word
Shift+Alt + → Expand selection
Shift+Alt + ← Shrink selection
Shift+Alt + drag mouse Column (box) selection
```
