# Supervisord

## Installation

Ubuntu

```shell
sudo apt-get install -y supervisor
```

## Configuration file

Configuration for gunicorn appliction that starts a flask app

```shell
[program:network]
directory=/home/cmills/Documents/home_net
command=/usr/local/bin/gunicorn --workers=3 network:app -b localhost:8000
autostart=true
autorestart=true
stderr_logfile=/var/log/network.err.log
stdout_logfile=/var/log/network.out.log
```

- place the configuration file in ```/etc/supervisor/conf.d/```.

## Start the application

```supervisorctl start network``` this will match the program name you indicated with the square brackets ```[program:network```]

## Get a status of running applications 

```supervisorctl status``` will give you an output of recognized applications that supervisor is aware of.

e.g.

```network                          RUNNING   pid 1205, uptime 0:49:06```

## Restart an applications

- trigger supervisor to reread the configuration or any new configuration file ```supervisorctl reread```

Note: If you modify the configuration file for any of your application you will have to restart supervisordst t

### Links

[Supervisord](http://supervisord.org)