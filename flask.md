## Installation files

### Install virtual environment for each flask app

     - sudo apt install python3-pip
     - sudo apt install python3-venv    - create virtual environments

### Create virtual environment

    - ``` python3 -m venv <destination directory>```

### Activate virtual environment

    - source ```venv/bin/Activate```

### Install dependencies

- ```pip -U install -r <path_to_requirements.txt_file>```

### Environment variables

There ere multiple ways to set and user variables used within a flask app

- use dotenv files namely .flaskenv and .env

- get any environment variables

  - os.environ.get('keys')

  - do this for all environment variables.

- Put all environment variables within a config file

        example ```/etc/config.json```

### restarting flask in debug mode

In your main app file "app.py" insert the following at the end of the file

```shell
if __name__ == '__main__':
    app.run(debug=True)
```

```python app.py```

### Deploying Flask

#### Create Firewall rules

- ```install ufw```

- ```apt install ufw```

- ```sudo ufw default allow outgoing```

- ```sudo ufw default deny incoming```

- ```sudo ufw allow ssh```

- ```sudo ufw allow 500 (Flask default port)```

- ```sudo ufw allow http/tcp```

- ```sudo ufw enable```

check status
```sudo ufw status```

### Deploy Flask application

use ```pip freeze >> requirements.txt``` to get a list of python packages needed for your flask environment
put all packages within an requirements.txt file to be used for installation.

### Create virtual environment on production server

- ```sudo apt install python3-pip```
- ```sudo apt install python3-venv```

```python3 -m venv flask_folder/venv```
```source flask_folder/venv/bin/activate```

#### Install requirements

```pip install -r path_to_requirements_file```

### Environment variables

Store environment variables within config.json file

```/etc/config.json``` (Name can be anything you want)

```json
{
    "SECRET_KEY": "5425452545254545"
    "DATABASE_KEY": "543251544255254"
}
```

### Production environment

```apt-get install nginx```

within virtual environment  install gunicorn

```pip install gunicorn```

### Nginx configuration

static files

- ```sudo rm /etc/nginx/sites-enabled/default```

- create configuration file /etc/nginx/sites-enables/flask_site_file

```json

server {
    listen 80;
    server_name IP address of the server;

    location /static {
        alias /location_of_flask_directory/locations_of_staic_files;
    }

    location / {
        proxy_pass http://localhost:8000; # forward python code to gunicorn
        include /etc/nginx/proxy_params;
        proxY_redirect off;
    }
}
```

### Gunicorn configuration

Python code

start gunicorn
```gunicorn -w 3 app:app```

- w  = workers "( 2 x num_of_cores_on_server) +1"
```nproc --all```

app:app = app after the colon is equal to the the app name

app.py or run.py or whatever you called it

``` python
from flaskblog import create_app

app = create_app()

if __name__ == '__main__':
    app.run()
```

### Start, autostart gunicorn using Supervisor

```sudo apt-get install supervisor```

sudo vi /etc/supervisor/conf.d/flask_app.conf "create file"

flask_app.conf

```
[program:flask_app_name]
directory=location_of_flask_directory
command=location_of_flask_directory/venv/bin/gunicorn -w 3 run:app
user=user_flask_running_under
autostart=true
autorestart=true
stopasgroup=true
killasgroup=true
stderr_logfile=/var/log/flask_app_dir/flask_app.err.log
stdout_logfile=/var/log/flask_app_dir/flask_app.out.log
```

```sudo supervisorctl reload```

#Create log file indicated within the configuration file#

### links

- [Using Flask](https://flask.palletsprojects.com/en/1.1.x/)
- [PrettyPrinted](https://prettyprinted.com/tutorials/automatically_load_environment_variables_in_flask)