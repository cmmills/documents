# NAgios

## Check cpu load

Manual check from nagios server

```/usr/local/nagios/libexec/check_nrpe -H va-kf01 -c check_load```

Nrpe configuration on monitoried host server

```/etc/nagiosn/nrpe.cfg``` nrpe configuration file

```command[check_load]=/usr/lib64/nagios/plugins/check_load -w 7,7,7 -c 9,9,9```

### Links

- [monitoring cpus](https://scoutapm.com/blog/understanding-load-averages)
- [What warning and critial values are used for check_load](https://serverfault.com/questions/209566/what-warning-and-critical-values-to-use-for-check-load)
- [Understanding CPU load](https://scoutapm.com/blog/understanding-load-averages)