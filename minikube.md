# Minikube

## installation

Install minikube and kubectl

### Install minikube
```curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 && mv minikube /usr/local/bin && chmod +x /usr/local/bin/minikube```

### Install kubectl

[how to install kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)

Install the latest version

```curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl```

Install a particular version, version 1.18.0

```curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.18.0/bin/linux/amd64/kubectl```

## Start minikube

```minikube start --driver=docker```

## Links

[installing Minikube](https://kubernetes.io/docs/setup/learning-environment/minikube/)