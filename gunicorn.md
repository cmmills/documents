# Gunicorn

The Green Unicorn, a python web server gateway interface HTTP server. https://gunicorn.org

## Installation

Install using pip ```pip install gunicorn``` or download from http://pypi.python.org/pypi/gunicorn/

## Deploying

Within your flask web directory execute the following command.

- ```gunicorn wsgi:app --reload```  use for testing when modifying your code.
- ```gunicorn wsgi:app``` used for normal deployment.
- ```gunicorn wsgi:app --bind 0.0.0.0:4000 or gunicorn wsgi:app -b :4000``` will launch gunicorn, and load your app on port 4000.

Using the above two commands gunicorn expects a configuration file (wsgi.py) similiar to the following located within the same directory. The configuration file provide instructions to gunicorn on how to load your application.

```
from oss_web import app

if __name__ == "__main__":
    app.run()
```

## View your application

View your application by going to http://127.0.0.1:8000, port 8000 is gunicorn's default port, if your did not specify a different port.

### Links

- [Gunicorn documentation](https://docs.gunicorn.org)
- [Setup Flask, Gunicorn, Nginx and Supervisor](https://medium.com/ymedialabs-innovation/deploy-flask-app-with-nginx-using-gunicorn-and-supervisor-d7a93aa07c18)