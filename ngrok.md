# Ngrok

Display local development web environment on the internet, tested with Flask and Gunicorn.
Download: ```https://ngrok.com/download```

#### Load htttp server

example: you want to review flask app running locally on port 8080 or gunicorn.
```ngrok http 8080``` substitute 800 for the port number your web server is currently listening on.

```                                                                                                                                                                                      
Session Status                online                                                                                                                                                  
Session Expires               7 hours, 59 minutes                                                                                                                                     
Version                       2.2.8                                                                                                                                                   
Region                        United States (us)                                                                                                                                      
Web Interface                 http://127.0.0.1:4040                                                                                                                                   
Forwarding                    http://310d0f5a.ngrok.io -> localhost:8080                                                                                                              
Forwarding                    https://310d0f5a.ngrok.io -> localhost:8080                                                                                                             
                                                                                                                                                                                      
Connections                   ttl     opn     rt1     rt5     p50     p90                                                                                                             
                              0       0       0.00    0.00    0.00    0.00   
```