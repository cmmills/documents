# Using Docker

## Docker commands

**list docker images:** ```docker images```

**list running containers:** ```docker ps```

**list past running containers:** ```docker ps -a```

**Run a docker image in detached (daemon mode):** ```docker run -dit-p 52022:21 -v --name=ubuntu-test ubuntu```

**List only certain parts of docker output - list only the names** ```docker ps --format '{{.Names}}```

**List only certain parts of docker output - list only the Status** ```docker ps --format '{{.Status}}```

**List only certain parts of docker output - list only the ID** ```docker ps --format '{{.ID}}```

**List only certain parts of docker output - list multiple options** ```docker ps --format '{{.Names}} - {{.Status}}```

**Attach to docker instance:** ```docker attach instance1```
- where instance1 is the name of the docker image.

- This will connect you into the container without a shell.

**Connect to a docker container:** 
```docker exec -it instance1 /bin/bash``` or
```docker attach instance1```

- this will connect you into the container to a shell.
- -it means connect interactively ```i=interactively``` to a terminal ```t=terminal```

**Stop a container:** ```docker stop instance1```

**Start a container:** ```docker start instance1```

**Restart a container:** ```docker restart instance1```

**Delete all non running docker containers by their ID's:** ```docker rm `docker ps -a -q` ```

 - running containers will generate an error indicating "stop the container before attempting removal"

**Bind a container port to a randome port locally on docker host:** ```docker run -d -P --name=instance1 ubuntu:latest```

**Bind a container port to a known port locally on the docker host:** ```docker run -d -p 8080:80 --name=instance1 ubuntu:latest```

**obtain container port:** ```docker port instance1 $CONTAINERPORT```

**Mount volumes within a docker container** ```docker run -d -v /mnt/data```

## Docker lifecycle

1. Start a docker container in daemon mode.
    - ```docker run -d -it --name=instance1 -h instance1ubuntu:16:04 /bin/bash```
        - -d - daemonize
        - -i - interactive
        - -t - open a pseudo terminal
        - --name - name of the container
        - -h = hostname

2. Connect / reconnect to as docker container.
     - ```docker exec -it instance1 /bin/bash```

3. Exit the docker container.
    - ```exit```

## Building containers

**Commamnd:**

```bash
docker build -t tag_name
```

### Dockerfile
 
 #### Run command 
 
 - connect multiple commands together instead of on a seperate line to avoid creating additional intermediate containers
 
    - ```RUN apt-get update && apt-get upgrade -y && apt-get install -y git httpd elinks```
 
 - The double ```ampersands``` causes the command to stop if any of the concatenated commands fail.

### ENV command

- Set an value that is accessible via the docker container.

### Expose command

- Exposes the conatiner port(s) to the underlying host.

  - ```Expose 80```

  - ```Expose 22```

### CMD command

- anything after the ```CMD``` is applied only on the base image
  - ```CMD ["/usr/sbin/apachectl","-d","FOREROUND"]```


### Exit Codes

Common exit codes associated with docker containers are:

- Exit Code 0: Absence of an attached foreground process
- Exit Code 1: Indicates failure due to application error
- Exit Code 137: Indicates failure as container received SIGKILL (Manual intervention or ‘oom-killer’ [OUT-OF-MEMORY])
- Exit Code 139: Indicates failure as container received SIGSEGV
- Exit Code 143: Indicates failure as container received SIGTERM

[Chroot exit codes](http://tldp.org/LDP/abs/html/exitcodes.html)