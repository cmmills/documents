# Netowrk

## List network  interfaces

``sudo lshw -c network```
output

```bash
*-network:0 DISABLED      
       description: Ethernet interface
       product: Ethernet Controller X710 for 10GbE SFP+
       vendor: Intel Corporation
       physical id: 0
       bus info: pci@0000:05:00.0
       logical name: enp5s0f0
       version: 02
       serial: 3c:fd:fe:9e:86:2c
       capacity: 10Gbit/s
       width: 64 bits
       clock: 33MHz
       capabilities: pm msi msix pciexpress vpd bus_master cap_list rom ethernet physical 1000bt-fd 10000bt-fd autonegotiation
       configuration: autonegotiation=off broadcast=yes driver=i40e driverversion=2.8.20-k firmware=5.05 0x80002927 1.1313.0 latency=0 link=no multicast=yes
       resources: irq:38 memory:c0800000-c0ffffff memory:c1c00000-c1c07fff memory:df580000-df5fffff memory:c0400000-c07fffff memory:c1c10000-c1d0ffff
  *-network:1 DISABLED
       description: Ethernet interface
       product: Ethernet Controller X710 for 10GbE SFP+
       vendor: Intel Corporation
       physical id: 0.1
       bus info: pci@0000:05:00.1
       logical name: enp5s0f1
       version: 02
       serial: 3c:fd:fe:9e:86:2e
       capacity: 10Gbit/s
       width: 64 bits
       clock: 33MHz
       capabilities: pm msi msix pciexpress vpd bus_master cap_list rom ethernet physical 1000bt-fd 10000bt-fd autonegotiation
       configuration: autonegotiation=off broadcast=yes driver=i40e driverversion=2.8.20-k firmware=5.05 0x80002927 1.1313.0 latency=0 link=no multicast=yes
       resources: irq:38 memory:c1000000-c17fffff memory:c1c08000-c1c0ffff memory:df500000-df57ffff memory:c1800000-c1bfffff memory:c1d10000-c1e0ffff
  *-network:0
       description: Ethernet interface
       product: 82576 Gigabit Network Connection
       vendor: Intel Corporation
       physical id: 0
       bus info: pci@0000:06:00.0
       logical name: enp6s0f0
       version: 01
       serial: e8:9a:8f:4f:db:f4
       size: 1Gbit/s
       capacity: 1Gbit/s
       width: 32 bits
       clock: 33MHz
```


## Links

- [Ubuntu Network](https://ubuntu.com/server/docs/network-configuration)