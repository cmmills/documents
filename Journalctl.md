# Journalctl


## View all logs

```journalctl```, this will display all journal entries

## Filter journal by time

Journals are organized by time, boot and date
- boots = journal entries from each time the server was booted

###  Show logs from boots

- list all boots```journalctl --list-boots```
- list boot from yesterday ```journalctl -b -1```

```journalctl --list-boots```

```bash
journalctl --list-boots
 0 1566584bf6974950b357045fb0d63f99 Fri 2020-05-22 18:15:50 EDT—Mon 2020-05-25 10:20:01 EDT
```

### Time period

- ```journalctl --since yesterday```
- ```journalctl --since 09:00 --until "1 hour ago"```
- ```journalctl --since "2018-01-21" --unitl "2020-05-20"```

## Filter by message interest 

Use this command to filter out applications
- ```journalctl -u crond.service``` or ```journalctl -u crond```
  
Filter by date
- ```journalctl -u docker.service --since today```


## Filter by Proces, User or Group ID

- ```journalctl _PID=1234```
- ```journalctl _PID=$(id -u nfsnobody)``` 
  - **id -u nfsnobody** figures out the ID for the nfsbody user.
  - **id -g nfsnobody** figures out the GID.


## Hacks

Don't wrpa lines

``` journalctl -k | less ``` or ```journalctl -k --no-pager```

### Links

[Digitalocean](https://www.digitalocean.com/community/tutorials/how-to-use-journalctl-to-view-and-manipulate-systemd-logs)
