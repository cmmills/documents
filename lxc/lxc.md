# LXC

## Install LXC

```snap install lxd``` or ```sudo apt-get install lxc```

## Enable and start lxc

```systemctl enable lxc``` ```systemctl start lx```

## where to find image

```lxc remote list```

```bash
 :> lxc remote list
+-----------------+------------------------------------------+---------------+-------------+--------+--------+
|      NAME       |                   URL                    |   PROTOCOL    |  AUTH TYPE  | PUBLIC | STATIC |
+-----------------+------------------------------------------+---------------+-------------+--------+--------+
| images          | https://images.linuxcontainers.org       | simplestreams | none        | YES    | NO     |
+-----------------+------------------------------------------+---------------+-------------+--------+--------+
| local (current) | unix://                                  | lxd           | file access | NO     | YES    |
+-----------------+------------------------------------------+---------------+-------------+--------+--------+
| ubuntu          | https://cloud-images.ubuntu.com/releases | simplestreams | none        | YES    | YES    |
+-----------------+------------------------------------------+---------------+-------------+--------+-------orage
| ubuntu-daily    | https://cloud-images.ubuntu.com/daily    | simplestreams | none        | YES    | YES    |
+-----------------+------------------------------------------+---------------+-------------+--------+--------+
```

Threee repositories configured by defalt when lxc is installd, images are pulled from the above repositories and stored within your local storage. 

## list you local images

```lxc image list```

```bash
 :> lxc image list
+-------+--------------+--------+---------------------------------------------+--------------+-----------+----------+-------------------------------+
| ALIAS | FINGERPRINT  | PUBLIC |                 DESCRIPTION                 | ARCHITECTURE |   TYPE    |   SIZE   |          UPLOAD DATE          |
+-------+--------------+--------+---------------------------------------------+--------------+-----------+----------+-------------------------------+
|       | 40cb36a2202d | no     | Alpine 3.13 amd64 (20210814_13:00)          | x86_64       | CONTAINER | 2.89MB   | Aug 14, 2021 at 11:52pm (UTC) |
+-------+--------------+--------+---------------------------------------------+--------------+-----------+----------+-------------------------------+
|       | fab57376cf04 | no     | ubuntu 20.04 LTS amd64 (release) (20210812) | x86_64       | CONTAINER | 366.21MB | Aug 13, 2021 at 4:32pm (UTC)  |
+-------+--------------+--------+---------------------------------------------+--------------+-----------+----------+-------------------------------+
```

## List remote images from the repositores.

```lxc image list images:``` This will list all images

search for an image ```lxc image list images:cent (use name linux name)```

"snippet"

```bash

 :> lxc image list images:
+--------------------------------------+--------------+--------+---------------------------------------------------+--------------+-----------------+-----------+-------------------------------+
|                ALIAS                 | FINGERPRINT  | PUBLIC |                    DESCRIPTION                    | ARCHITECTURE |      TYPE       |   SIZE    |          UPLOAD DATE          |
+--------------------------------------+--------------+--------+---------------------------------------------------+--------------+-----------------+-----------+-------------------------------+
| almalinux/8 (3 more)                 | 5a0f1257e670 | yes    | Almalinux 8 amd64 (20210805_23:08)                | x86_64       | CONTAINER       | 126.17MB  | Aug 5, 2021 at 12:00am (UTC)  |
+--------------------------------------+--------------+--------+---------------------------------------------------+--------------+-----------------+-----------+-------------------------------+
| almalinux/8 (3 more)                 | be8cb874d36e | yes    | Almalinux 8 amd64 (20210805_23:08)                | x86_64       | VIRTUAL-MACHINE | 587.44MB  | Aug 5, 2021 at 12:00am (UTC)  |
+--------------------------------------+--------------+--------+---------------------------------------------------+--------------+-----------------+-----------+-------------------------------+
| almalinux/8/arm64 (1 more)           | ad3f8daa51ee | yes    | Almalinux 8 arm64 (20210805_23:08)                | aarch64      | CONTAINER       | 122.20MB  | Aug 5, 2021 at 12:00am (UTC)  |
+--------------------------------------+--------------+--------+---------------------------------------------------+--------------+-----------------+-----------+-------------------------------+
| almalinux/8/cloud (1 more)           | 7cfd5e80a47a | yes    | Almalinux 8 amd64 (20210805_23:08)                | x86_64       | CONTAINER       | 145.22MB  | Aug 5, 2021 at 12:00am (UTC)  |
+--------------------------------------+--------------+--------+---------------------------------------------------+--------------+-----------------+-----------+-------------------------------+
| almalinux/8/cloud (1 more)           | d042f262cd05 | yes    | Almalinux 8 amd64 (20210805_23:08)                | x86_64       | VIRTUAL-MACHINE | 627.06MB  | Aug 5, 2021 at 12:00am (UTC)  |
+--------------------------------------+--------------+--------+---------------------------------------------------+--------------+-----------------+-----------+-------------------------------+
| almalinux/8/cloud/arm64              | af19c788cc7c | yes    | Almalinux 8 arm64 (20210805_23:08)                | aarch64      | CONTAINER       | 141.19MB  | Aug 5, 2021 at 12:00am (UTC)  |
+--------------------------------------+--------------+--------+---------------------------------------------------+--------------+-----------------+-----------+-------------------------------+
| alpine/3.11 (3 more)                 | de3a40045f43 | yes    | Alpine 3.11 amd64 (20210805_13:00)                | x86_64       | CONTAINER       | 2.41MB    | Aug 5, 2021 at 12:00am (UTC)  |
+--------------------------------------+--------------+--------+---------------------------------------------------+--------------+-----------------+-----------+-------------------------------+
| alpine/3.11 (3 more)                 | ecbe19e45bb4 | yes    | Alpine 3.11 amd64 (20210805_13:00)                | x86_64       | VIRTUAL-MACHINE | 83.81MB   | Aug 5, 2021 at 12:00am (UTC)  |
+--------------------------------------+--------------+--------+---------------------------------------------------+--------------+-----------------+-----------+-------------------------------+
| alpine/3.11/arm64 (1 more)           | 6a6921a11a7a | yes    | Alpine 3.11 arm64 (20210805_13:00)                | aarch64      | CONTAINER       | 2.22MB    | Aug 5, 2021 at 12:00am (UTC)  |
+--------------------------------------+--------------+--------+---------------------------------------------------+--------------+-----------------+-----------+-------------------------------+
```

## launch an image

To launch ubuntu based images you just need to use ```lxc launch ubuntu:<version>```, the reason for that is the name of the image repository is stated in the remote list and you just need to specify the image version example: ```lxc launch ubuntu:21.04 ubuntu2104``` will launch a ubuntu 21.04 image with the name  ```*ubuntu*```.
 
```lxc launch ubuntu:20.04``` if you don't specify a name it will assign a random name

## log into an instance

```lxc exec c1 -- /bin/bash```

### log into an instance as a specifig user

```lxc exec ubuntuone -- su --login devops```

### Log in via console

```bash
:> lxc console ubuntuone
To detach from the console, press: <ctrl>+a q

ubuntuone login: devops
Password: 
```

## list your local storage

```lxc storage list```

```bash
 :> lxc storage list
+---------+-------------+--------+--------------------------------------------+---------+
|  NAME   | DESCRIPTION | DRIVER |                   SOURCE                   | USED BY |
+---------+-------------+--------+--------------------------------------------+---------+
| default |             | zfs    | /var/snap/lxd/common/lxd/disks/default.img | 5       |
+---------+-------------+--------+--------------------------------------------+---------+
```

[profile](profile.md)


## Links

- [https://linuxcontainers.org/lxd/getting-started-cli](Getting Started CLI)
- [https://linuxcontainers.org/lxd/advanced-guide/](Advanced Guide)
- [https://blog.simos.info/how-to-run-a-windows-virtual-machine-on-lxd-on-linux/](Install windows 10 on LXC)