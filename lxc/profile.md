# LXC Profile

## List profiles

```lxc profile list```

```bash
profile list
+---------+---------+
|  NAME   | USED BY |
+---------+---------+
| default | 1       |
+---------+---------+
| mills1  | 0       |
+---------+---------+
```

### create a new profile


#### Show default profile

```bash
 profile show default
config: {}
description: Default LXD profile
devices:
  eth0:
    name: eth0
    network: lxdbr0
    type: nic
  root:
    path: /
    pool: default
    type: disk
name: default
used_by:
- /1.0/instances/ubuntucloudfocal
```

#### Show mills1 profile

```bash
 :> lxc profile show mills1
config:
  user.user-data: "#cloud-config\nusers:\n  - name: devops\n    username: devops\n
    \   groups: sudo\n    gecos: DevOps user\n    passwd: $6$qwerty$WyR1Se0Elh217J/vImk32MWjHtrku.bKs6NtloKgR7LNBBO3tR8bcpYM3YF2pwHO3uFluK3MHxN81ioMQrZhO.\n
    \   lock_passwd: false\n    sudo: ['ALL=(ALL) NOPASSWD:ALL']\n    shell: /bin/bash\n
    \   groups: sudo\n    ssh_authorized_keys:\n    - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCrEIt4W0QziaTg340jR2vQnZceV3890WxnoC7oSmAOq5EPWjMzXbceJjGSneC6D9ZH8YiX4gXKwW3nXNDoF7Fb98h54R3wxbSVuRFXAxy6W3+C9vZxXf28WEO7f8ACrRwmvwc8VNiFcjL80UbTC13YzAc07bUHJBcDUopLSL9vo1s3mfAm1T6KJyADkUIzhjEI8v1ucQjn1XGoRDKKfBm5dx8/B/k1vwp7XUJtaIK/7zyMqx/eV/VEjh9jW74utcslJPHCmpjdcs+CJkRqUDOOSP/eRTWiCJcwhxBKWkwXDaYqjdO/p3kJFXBeyIfTP6xlOl2QdelT4pZtVu7
    ansible-generated on va-jump-1\n    - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC9YdhxDvXlAgKm/M3X++tGtatSAKJtBpuTIzvP+wUV9mbNJqkWU0fMUGFGkEKX1vlLJaMtqLju+Z/t5vfVyRgeLnPMZLV/Ixkq21AhbgK9Ano1RB6ukEtzJ1aDb4BLLLEG/vLf0eKJDsRA6q/0xczvgkCiFL7IQXA0TJW6MBrx03OPA/FHg2Z6ei8bhwoo5Acibe5f0gBwiQ71KVx8KQ602ShqCutayMSdyWPPhRRGZ2D4XUCz+Oz6ZuGapX2PE0eJihb8HCtbtDYydrMgYd6hxABSq0xxxODmNipqmFEWShpP0b9mWjfEIAO1/QqwL96vgixBTCIghedm4qQQDF0hqhWI1YtTYFbN3CIQRn1dMKF5i4slPsTMclQOLybwlptzs7Jd/boyZ6594CIDonppTK+4RrUmtFQOofLZxVg6jG8sgBTzDOp8D+zTRj47XaZE/zDgJMVlwxoutTNXtVjTw1QYEZevhp+sBs9z96K2HyooV2FOfoMMjib9hODvzMD9EnwZwQkegCdLnjlaTTVUXrRjem2hXjpCdUCp0cZ+y8mQ1JIfoGBZtU3fAPuna9HZO4JqpTcjX9CpdCH/QbIIGcwD9XifiIrbstzRS7Rl7zKj1+z4V/EjfoQGPWMJjjpCPRSHal9Zk0yLQ2pYo0VXDxgafdrQYlPrZod+9zHmRw==
    itops-20171024\n\n# Create new SSHD configuration file\nwrite_files:\n  - path:
    /etc/ssh/sshd_config\n    content: |\n        Include /etc/ssh/sshd_config.d/*.conf\n
    \       ChallengeResponseAuthentication no\n        UsePAM yes\n        X11Forwarding
    yes\n        PrintMotd no\n        AcceptEnv LANG LC_*\n        Subsystem sftp
    \ /usr/lib/openssh/sftp-server \n        PasswordAuthentication yes\n\n# Restart
    SSHD to apply the sshd settings\nruncmd:\n  - systemctl restart sshd\n\n# Update
    MOTD\nwrite_files:\n  - path: /etc/motd\n    content: |\n        K8s Hadoop cluster\n
    \   owner: root:root\n    permissions: 0644\n\n# update all packages\npackage_upgrade:
    true\npackages:\n  - net-tools\n  - wget\n  - mlocate\n  - unzip\n  - rsync\n
    \ - mtr\n  - ntp\n  - iftop\n  - nagios-nrpe-server \n  - nagios-plugins\n\n#
    Set timezone\ntimezone: America/New_York"
description: ""
devices:
  eth0:
    name: eth0
    network: lxdbr0
    type: nic
  root:
    path: /
    pool: default
    type: disk
name: mills1
used_by: []
```

## Create a LXC profile

```cp cloud-config.cfg cloud-profile.profile```

## Cloud-init create config

### Update cloud config profle

edit the file created above and adde the following to the top of the file

```bash
config:
  user.user-data: |  
    #cloud-config
    users:
```

### Create a LXC profile

```lxc profile create mills1```

### Add cloud init file to lxc profile

```cat cloud-profile.profile | lxc profile edit mills1``` - copy an existing cloud-int file 

### Apply the profile to an instance

```lxc launch ubuntu:20.04 ubuntuone -p mills1```

[lxc](lxc.md)