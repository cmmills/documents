# WIFI setup

## Find you interface name

### Install wireless tools

```   60  sudo apt install wireless-tools-y ```                                                                                                                                                    


```iwconfig```

output

```bash
veth05ab693e  no wireless extensions.

wlp58s0   IEEE 802.11  ESSID:"somenetwork"  
          Mode:Managed  Frequency:5.785 GHz  Access Point: F4:C1:14:8E:92:F3   
          Bit Rate=866.7 Mb/s   Tx-Power=22 dBm   
          Retry short limit:7   RTS thr:off   Fragment thr:off
          Power Management:on
          Link Quality=51/70  Signal level=-59 dBm  
          Rx invalid nwid:0  Rx invalid crypt:0  Rx invalid frag:0
          Tx excessive retries:0  Invalid misc:8   Missed beacon:0

wlx74da3855fb8c  IEEE 802.11  ESSID:off/any  
          Mode:Managed  Access Point: Not-Associated   Tx-Power=0 dBm   
          Retry short limit:7   RTS thr=2347 B   Fragment thr:off
          Power Management:on
          
eno1      no wireless extensions.

lxdbr0    no wireless extensions.

lo        no wireless extensions.
```

the wireless interface is called ```wlp58s0``` and this interface is already connected to a wifi network "somenetwork"


## Find your network name

snippet
```bash
                    ESSID:"Rogers1_EXT"
                    ESSID:""
                    ESSID:"Event Horizon 2.4"
                    ESSID:"FBI"
                    ESSID:"Aitkenhouse"
                    ESSID:"Rogers16633-5G"
                    ESSID:"TML"
```

## Connect to your WI-FI with wap_supplicant

```sudo apt install wpasupplicant```

You will need to create a wpa_supplicant.conf file using wpa_passphrase utility, wpa_supplicatnt.conf is the configuration file describing the networks the computer needs to connect to.

```wpa_passphrase *your-ESSID your-wifi-passphrase* | sudo tee /etc/wap_supplicant.conf```

Once the configuration file is created, you can use the following command to connect to your wifi network.

```sudo wpa_supplicant -B -c /etc/wpa_supplicant.conf -i wlp58s0```

-B sends the command in the background
-c path to the wpa_supplicant configuration file.

once you have authenticated to the network, you still need to get an IP 

```sudo dhclient wlp58s0```

## Auto-Connect At Boot Time
To automatically connect to wireless network at boot time, we need to edit the wpa_supplicant.service file. It’s a good idea to copy the file from /lib/systemd/system/ directory to /etc/systemd/system/ directory, then edit the file content, because we don’t want a newer version of wpa_supplicant to override our modifications.

```sudo cp /lib/systemd/system/wpa_supplicant.service /etc/systemd/system/wpa_supplicant.service```
Edit the file with a command-line text editor, such as Nano.

```sudo vi /etc/systemd/system/wpa_supplicant.service```
Find the following line.

```ExecStart=/sbin/wpa_supplicant -u -s -O /run/wpa_supplicant```
Change it to the following. Here we added the configuration file and the wireless interface name to the ExecStart command.

```ExecStart=/sbin/wpa_supplicant -u -s -c /etc/wpa_supplicant.conf -i wlp58s0```
It’s recommended to always try to restart wpa_supplicant when failure is detected. Add the following right below the ExecStart line.

```Restart=always```
If you can find the following line in this file, comment it out (Add the # character at the beginning of the line).

```Alias=dbus-fi.w1.wpa_supplicant1.service```
Save and close the file. (To save a file in Nano text editor, press Ctrl+O, then press Enter to confirm. To exit, press Ctrl+X.) Then reload systemd.

```sudo systemctl daemon-reload```
Enable wpa_supplicant service to start at boot time.

```sudo systemctl enable wpa_supplicant.service```
We also need to start dhclient at boot time to obtain a private IP address from DHCP server. This can be achieved by creating a systemd service unit for dhclient.

```sudo vi /etc/systemd/system/dhclient.service```
Put the following text into the file.
```bash
[Unit]
Description= DHCP Client
Before=network.target
After=wpa_supplicant.service

[Service]
Type=forking
ExecStart=/sbin/dhclient wlp4s0 -v
ExecStop=/sbin/dhclient wlp4s0 -r
Restart=always
 
[Install]
WantedBy=multi-user.target
Save and close the file. Then enable this service.

sudo systemctl enable dhclient.service
```

### Link
- [Connect to wifi termainl on Ubuntu 18:04 and 20:04](https://www.linuxbabe.com/ubuntu/connect-to-wi-fi-from-terminal-on-ubuntu-18-04-19-04-with-wpa-supplicant)