# Node

## Package.json file

The package.json file is created by using the command ```npm init``` you will be asked some questions, git repo, author name etc. One of the usage for package.json file is to store you application dependecies.

```bash
 "dependencies": {
    "mysql2": "^2.2.5"
  },
```

You should store this file within your version control system as you can use this file to re-install all dependencies that were installed using npm. You can then use ```npm install``` command to re-install those files as long as you have the package.json file present, this will recreate the node_modules directory with the modules and their dependencioes listed within.

## Naming your ap

if you name your file ```index.js``` within any folder you can then just execute the app by using the following command ```node <directory_name>``

## Access environment variables

```bash
:> export mysql_user=root
cmills@HP-ENVY: 2020-12-26 ~

 :> node
> console.log(process.env.mysql_user);
root
```
