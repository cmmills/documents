# GPG How to

Use the following link [to sign your key](https://git-scm.com/book/en/v2/Git-Tools-Signing-Your-Work)

## Generating gpg keys

  ```gpg --full-gen-key```

## list key

``` shell
  gpg --list-secret-keys
  ggp --list-keys
  ```

## export public gpg key

``` shell
  gpg --armor --export "clarence.mills@gmail.com (use email to locate key)"
  gpg --armor --export 7ff00EA (short key ID)

```

## Change gpg key expiration Date

``` shell
:> gpg --edit-key 7DF546C8
gpg (GnuPG) 2.2.4; Copyright (C) 2017 Free Software Foundation, Inc.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

Secret key is available.

sec  rsa4096/211198807DF546C8
     created: 2017-10-13  expired: 2019-10-13  usage: SC  
     trust: unknown       validity: expired
ssb  rsa4096/435E79FA00F9CF23
     created: 2017-10-13  expired: 2019-10-13  usage: E   
[ expired] (1). Clarence Mills (Clarence Mills) <clarence.mills@gmail.com>

gpg> expire
Changing expiration time for the primary key.
Please specify how long the key should be valid.
         0 = key does not expire
      <n>  = key expires in n days
      <n>w = key expires in n weeks
      <n>m = key expires in n months
      <n>y = key expires in n years
Key is valid for? (0) 1y
Key expires at Wed 21 Oct 2020 10:53:21 AM EDT
Is this correct? (y/N) y

sec  rsa4096/211198807DF546C8
     created: 2017-10-13  expires: 2020-10-21  usage: SC  
     trust: unknown       validity: unknown
ssb  rsa4096/435E79FA00F9CF23
     created: 2017-10-13  expired: 2019-10-13  usage: E   
[ unknown] (1). Clarence Mills (Clarence Mills) <clarence.mills@gmail.com>

gpg: WARNING: Your encryption subkey expires soon.
gpg: You may want to change its expiration date too.
gpg> q
Save changes? (y/N) y
```


## Configure GIT

This command instructs git to sign with your GPG key

  ``` shell
  gpg config --global user.signingkey YOUR_KEY_ID
  ```

This command instructs git to automatically sign all commits with the key entered above.
```bash
git config --global commit.gpgsign true
```

Alternatively you can sign commits manually.

  ```git commit -S -m commit message```    Capital "-S" after commit is what is the option to sign commits.

To make auto signing work pre git version 2.0, you'll have to add git alias for commit.

```bash
git config --global alias.commit commit -S
[alias]
    commit = commit -S
```
  
- You will be required to enter your pass-phrase

