# Yum

- Yum configuration file is located at ```/etc/yum.conf```
- Yum repositories are located in ```/etc/yum.repos.d/```

## <a name="contents">Contents</a>

- [Yum repolist](#yum_repo)
- [List detailed infromation about a yum repository](#list_detail_package_info)
- [Disable a yum repository](#disable_yum_repo)
- [Temporarily disable a repo during an update](#temp_disable)
- [Enable a yum repository](#enable_repo)
- [Permanently disable a repository](#perm_disable_repo)
- [View YUM history](#view_yum_history)
- [show detail history information about a package](#show_detail_history)
- [View available updates for a package](#view_available_update_for_package)
- [Exclude packages](#exclude_packages)
- [list available packages from a yum repository](#list_available_packages)

## <a name="yum_repo">Yum repolist</a>

Adding a repository to a system is done by addding a ```[repository]``` section to the ```/etc/yum.conf``` file or to add ```<repository_name>.repo``` file to ```/etc/yum.repos.d``` directory.n It's recommended to add *.repo files to ```/etc/yum.repos.d/``` directory.

- list avaiable repositories installed on a system
command: ```yum repolist```

example:

```shell

Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
 * base: ewr.edge.kernel.org
 * epel: fedora-epel.mirrors.tds.net
 * extras: mirror.ash.fastserv.com
 * remi-safe: repo1.ash.innoscale.net
 * updates: mirror.ash.fastserv.com
repo id                                                               repo name                                                                                        status
!base/7/x86_64                                                        CentOS-7 - Base                                                                                  10,097
!epel/x86_64                                                          Extra Packages for Enterprise Linux 7 - x86_64                                                   13,249
!extras/7/x86_64                                                      CentOS-7 - Extras                                                                                   341
!updates/7/x86_64                                                     CentOS-7 - Updates                                                                                1,787
```

[Back to top](#contents)

## <a name="list_detail_package_info">List detailed infoamtion about a yum repository</a>

command: ```yum -v repolist```

example
```shell

Repo-id      : epel/x86_64
Repo-name    : Extra Packages for Enterprise Linux 7 - x86_64
Repo-revision: 1587334434
Repo-updated : Sun Apr 19 18:17:04 2020
Repo-pkgs    : 13,249
Repo-size    : 15 G
Repo-metalink: https://mirrors.fedoraproject.org/metalink?repo=epel-7&arch=x86_64
  Updated    : Sun Apr 19 18:17:04 2020
Repo-baseurl : http://fedora-epel.mirrors.tds.net/fedora-epel/7/x86_64/ (37
             : more)
Repo-expire  : 21,600 second(s) (last: Mon Apr 20 11:00:15 2020)
  Filter     : read-only:present
Repo-filename: /etc/yum.repos.d/epel.repo
```

## <a name="disable_yum_repo">Disable a yum repository</a>

Install ```yum install yum-utils``` to get access to ```yum-config-manager``` command if it's not on the system.

```yum-config-manager --disable <repository name>```

example: disable epel repository

```yum-config-manager --disable epel```

[Back to top](#contents)

## <a name="temp_disable">Temporarily disable a repo during an update</a>

```yum --disablerepo=<repository name> update```

eample: ```yum --disablerepo=epel update```

To disable multiple repository seperate each repository by a comma.

## <a name="enable_repo">Enable a yum repository</a>

```yum-config-manager --enable epel```

[Back to top](#contents)

## <a name="perm_disable_repo">Permanently disable a repository</a>

Edit the the repository file located within ```/etc/yum.repos.d/epel.repo```

locate the value **enabled=1** and change to **enable=0**

[Back to top](#contents)

## <a name="view_yum_history">View YUM history</a>

command: ```yum history```

```shell
# yum history
 Loaded plugins: fastestmirror
    ID     | Login user               | Date and time    | Action(s)      | Altered
    -------------------------------------------------------------------------------
         8 | Open ... <oss>           | 2020-04-20 00:29 | Install        |   11
         7 | Open ... <oss>           | 2020-04-20 00:03 | I, U           |  125 EE
         6 | root <root>              | 2020-04-19 16:17 | I, U           |    5
         5 | root <root>              | 2020-04-19 16:16 | I, U           |    5
         4 | root <root>              | 2015-08-18 18:32 | Install        |    2
         3 | root <root>              | 2015-08-18 18:31 | Install        |    1
         2 | root <root>              | 2015-08-18 18:22 | I, U           |   24
         1 | System <unset>           | 2015-08-18 17:31 | Install        |  205
    history list

```

[Back to top](#contents)

## <a name="show_detail_history">show detail history information about a package</a>

**history sub-commands are:** ```info/list/summary```

command: ```yum  history info docker```

```shell
Loaded plugins: fastestmirror
Transaction ID : 65
Begin time     : Tue Dec 11 22:10:55 2018
Begin rpmdb    : 735:7c6c36612d2b35fcefc8dd0899d84898ea221dbc
End time       :            22:11:02 2018 (7 seconds)
End rpmdb      : 735:cbc914d67f835f570b0f644fdc7113c663d80903
User           : root <root>
Return-Code    : Success
Command Line   : update -y
Transaction performed with:
    Installed     rpm-4.11.3-35.el7.x86_64                      @base
    Installed     yum-3.4.3-161.el7.centos.noarch               @base
    Installed     yum-metadata-parser-1.1.4-10.el7.x86_64       @anaconda
    Installed     yum-plugin-fastestmirror-1.1.31-50.el7.noarch @base
Packages Altered:
    Updated docker-2:1.13.1-84.git07f3374.el7.centos.x86_64        @extras
    Update         2:1.13.1-88.git07f3374.el7.centos.x86_64        @extras
    Updated docker-client-2:1.13.1-84.git07f3374.el7.centos.x86_64 @extras
    Update                2:1.13.1-88.git07f3374.el7.centos.x86_64 @extras
    Updated docker-common-2:1.13.1-84.git07f3374.el7.centos.x86_64 @extras
    Update                2:1.13.1-88.git07f3374.el7.centos.x86_64 @extras
-------------------------------------------------------------------------------
Transaction ID : 64
Begin time     : Tue Dec  4 17:28:20 2018
Begin rpmdb    : 708:210def9d5e7bc9f610f1441090c072ef47021e43
End time       :            17:28:34 2018 (14 seconds)
End rpmdb      : 735:7c6c36612d2b35fcefc8dd0899d84898ea221dbc
User           : root <root>
Return-Code    : Success
Command Line   : install -y docker python-pip
Transaction performed with:
    Installed     rpm-4.11.3-35.el7.x86_64                      @base
    Installed     yum-3.4.3-161.el7.centos.noarch               @base
    Installed     yum-metadata-parser-1.1.4-10.el7.x86_64       @anaconda
    Installed     yum-plugin-fastestmirror-1.1.31-50.el7.noarch @base
Packages Altered:
    Dep-Install PyYAML-3.10-11.el7.x86_64                                          @base
    Dep-Install atomic-registries-1:1.22.1-26.gitb507039.el7.centos.x86_64         @extras
    Dep-Install audit-libs-python-2.8.4-4.el7.x86_64                               @base
    Dep-Install checkpolicy-2.5-8.el7.x86_64                                       @base
    Dep-Install container-selinux-2:2.74-1.el7.noarch                              @extras
    Dep-Install container-storage-setup-0.11.0-2.git5eaf76c.el7.noarch             @extras
    Dep-Install containers-common-1:0.1.31-7.gitb0b750d.el7.centos.x86_64          @extras
    Install     docker-2:1.13.1-84.git07f3374.el7.centos.x86_64                    @extras
    Dep-Install docker-client-2:1.13.1-84.git07f3374.el7.centos.x86_64             @extras
    Dep-Install docker-common-2:1.13.1-84.git07f3374.el7.centos.x86_64             @extras
    Dep-Install libcgroup-0.41-20.el7.x86_64                                       @base
    Dep-Install libseccomp-2.3.1-3.el7.x86_64                                      @base
    Dep-Install libsemanage-python-2.5-14.el7.x86_64                               @base
    Dep-Install libyaml-0.1.4-11.el7_0.x86_64                                      @base
    Dep-Install oci-register-machine-1:0-6.git2b44233.el7.x86_64                   @extras
    Dep-Install oci-systemd-hook-1:0.1.18-2.git3efe246.el7.x86_64                  @extras
    Dep-Install oci-umount-2:2.3.4-2.git87f9237.el7.x86_64                         @extras
    Dep-Install policycoreutils-python-2.5-29.el7.x86_64                           @base
    Dep-Install python-IPy-0.75-6.el7.noarch                                       @base
    Dep-Install python-backports-1.0-8.el7.x86_64                                  @base
    Dep-Install python-backports-ssl_match_hostname-3.5.0.1-1.el7.noarch           @base
    Dep-Install python-ipaddress-1.0.16-2.el7.noarch                               @base
    Dep-Install python-pytoml-0.1.14-1.git7dea353.el7.noarch                       @extras
    Dep-Install python-setuptools-0.9.8-7.el7.noarch                               @base
    Install     python2-pip-8.1.2-6.el7.noarch                                     @epel
    Dep-Install setools-libs-3.3.8-4.el7.x86_64                                    @base
    Dep-Install subscription-manager-rhsm-certificates-1.21.10-3.el7.centos.x86_64 @updates
Scriptlet output:
   1 setsebool:  SELinux is disabled.
history info
```

[Back to top](#contents)

## <a name="view_available_update_for_package">View available updates for a package</a>

You have a couple of options

command: ```yum --showduplicates list '<package name>'```

example: ```yum --showduplicates list 'docker'```

```shell
Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
 * base: mirror.datto.com
 * epel: fedora-epel.mirrors.tds.net
 * extras: mirror.cc.columbia.edu
 * remi-safe: repo1.ash.innoscale.net
 * updates: mirror.metrocast.net
Installed Packages
docker.x86_64                                                           2:1.13.1-88.git07f3374.el7.centos                                                             @extras
Available Packages
docker.x86_64                                                           2:1.13.1-102.git7f2769b.el7.centos                                                            extras 
docker.x86_64                                                           2:1.13.1-103.git7f2769b.el7.centos                                                            extras 
docker.x86_64                                                           2:1.13.1-108.git4ef4b30.el7.centos                                                            extras 
docker.x86_64                                                           2:1.13.1-109.gitcccb291.el7.centos                                                            extras 
```

**or**

command: ```yum list available <package name>```

example: ```yum list available docker\*``

Add ```\*" to get available updates for all docker related packages.

```shell
Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
 * base: mirror.datto.com
 * epel: fedora-epel.mirrors.tds.net
 * extras: mirror.cc.columbia.edu
 * remi-safe: repo1.ash.innoscale.net
 * updates: mirror.metrocast.net
Available Packages
docker.x86_64                                                                       2:1.13.1-109.gitcccb291.el7.centos                                                 extras
docker-client.x86_64                                                                2:1.13.1-109.gitcccb291.el7.centos                                                 extras
docker-client-latest.x86_64                                                         1.13.1-58.git87f2fab.el7.centos                                                    extras
docker-common.x86_64                                                                2:1.13.1-109.gitcccb291.el7.centos                                                 extras
docker-compose.noarch                                                               1.18.0-4.el7                                                                       epel  
docker-distribution.x86_64                                                          2.6.2-2.git48294d9.el7                                                             extras
docker-latest.x86_64                                                                1.13.1-58.git87f2fab.el7.centos                                                    extras
docker-latest-logrotate.x86_64                                                      1.13.1-58.git87f2fab.el7.centos                                                    extras
docker-latest-v1.10-migrator.x86_64                                                 1.13.1-58.git87f2fab.el7.centos                                                    extras
docker-logrotate.x86_64                                                             2:1.13.1-109.gitcccb291.el7.centos                                                 extras
docker-lvm-plugin.x86_64                                                            2:1.13.1-109.gitcccb291.el7.centos                                                 extras
docker-novolume-plugin.x86_64                                                       2:1.13.1-109.gitcccb291.el7.centos                                                 extras
docker-registry.x86_64                                                              0.9.1-7.el7                                                                        extras
docker-v1.10-migrator.x86_64                                                        2:1.13.1-109.gitcccb291.el7.centos                                                 extras

```

[Back to top](#contents)

## <a name="exclude_packages">Exclude a single package when updating</a>

command: ```yum update --exclude=docker``` 

 or  
 
 ```yum update -x 'kernel*'```

## Exclude multiple packages from updating

command: ```yum update --exclude=kernel* --exclude=docker```

Update the system avoiding updating the kernel and docker packages.

## <a name="list_available_packages">list available packages from a yum repository</a>

command: ```yum --disablerepo="*" --enablerepo="epel" list-available```

- the first command ```--disablerepo="*" ```  - basically disables all repositories,  you can use * to remove everythin or use ``repo id`` or ```global id```
- the second command ```--enablerepo="epel"``` enables one repository, in this case the repository labelled epel.
- ```list-available``` list all available packages within the repository.

[Back to top](#contents)

Links:

- [Redhat Support](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/deployment_guide/sec-managing_yum_repositories)
- [Unix men](https://www.unixmen.com/enable-disable-repositories-centos/)
- [Like Geeks](https://likegeeks.com/yum-update-command/)
- [Techmint](https://www.tecmint.com/view-yum-history-to-find-packages-info/)
- [Stackexchange](https://unix.stackexchange.com/questions/75981/yum-check-available-package-updates)

[Back to top](#contents)
