# Using GRUB

Boot menu for booting kernels

- grub and grub2 are the same commands.

- Primary use of the commands are when you have updated/added a new kernel to the system.

- Do not modify the grub configration file manually, use grub configuration commands.

## Create new grub.cfg configuration file

```grub2-mkconfig -o /boot/grub2/grub.cfg```

-o option is points to the file location where you want the output( by default outputs to stdout) from the ```grub2-mkconfig``` command to go.

## Modifying the boot menu

### list the boot menu order

Grub configuration file is located at ```/etc/grub2.cfg```

To view the current grub boot menu using either of the following commands.

```awk -F\' '$1=="menuentry " {print i++ " : " $2}' /etc/grub2.cfg```

```shell 
[root@localhost ~]# awk -F\' '$1=="menuentry " {print i++ " : " $2}' /etc/grub2.cfg
0 : CentOS Linux (5.6.3-1.el7.elrepo.x86_64) 7 (Core)
1 : CentOS Linux (3.10.0-123.el7.x86_64) 7 (Core)
2 : CentOS Linux (0-rescue-7b35116927d74ea58785e00b47ac0f0d) 7 (Core)
```

or

```grep "^menuentry" /boot/grub2/grub.cfg | cut -d "'" -f2```

```shell
[root@localhost ~]# grep "^menuentry" /boot/grub2/grub.cfg | cut -d "'" -f2
CentOS Linux, with Linux 3.10.0-123.el7.x86_64
CentOS Linux, with Linux 0-rescue-7b35116927d74ea58785e00b47ac0f0d
```

### Show currently set kernel to boot from

```shell
root@localhost ~]# grub2-editenv list
saved_entry=CentOS Linux, with Linux 3.10.0-123.el7.x86_64
```

### Set new boot order

Once you have a list of the boot menu items using the comands from that last section, you can modify the boot menu option using the command below.

```grub2-set-default 0```

```shell
[root@localhost ~]# grub2-set-default 0
[root@localhost ~]# awk -F\' '$1=="menuentry " {print i++ " : " $2}' /etc/grub2.cfg
0 : CentOS Linux (5.6.3-1.el7.elrepo.x86_64) 7 (Core)
1 : CentOS Linux (3.10.0-123.el7.x86_64) 7 (Core)
2 : CentOS Linux (0-rescue-7b35116927d74ea58785e00b47ac0f0d) 7 (Core)
```

#### Verfiy what kernel is now set

```grub2-editenv list```

```shell
[root@localhost ~]#  grub2-editenv list
saved_entry=0

```

When next you restart/reboot your server it will automatically load the kernel from option 0 from above, option 0 will be the new kernel the will be booted into.

### Temporarily set the server to boot a different kernel.

This command ```grub2-reboot 1``` will set the grub boot menu to boot the kernel located at option [1] on the next restart or reboot only once. The second restart/reboot the server will boot it default kernel wet witin grub configuration.

links

- [Grub rescue](https://linuxhint.com/grub_rescue_commands_centos/) 
- [Grub2 - centos](https://wiki.centos.org/HowTos/Grub2)
- [Gnu org GRUB](https://www.gnu.org/software/grub/manual/grub/grub.html)
- [How to upgrade your kernel](https://phoenixnap.com/kb/how-to-upgrade-kernel-centos)
- [Mkconfig tutorial -  linuxhint](https://linuxhint.com/grub2_mkconfig_tutorial/)
- [linux kernel archive](https://www.kernel.org/)
