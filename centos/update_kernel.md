# Upgrade kernel

## How to upgrade kernel

- sudo yum update -y
- rpm --import https://www.elrepo.org/RPM-GPG-KEY-elrepo.org
- rpm -Uvh https://www.elrepo.org/elrepo-release-7.0-3.el7.elrepo.noarch.rpm
- yum list available --disablerepo='*' --enablerepo=elrepo-kernel
- yum --enablerepo=elrepo-kernel install kernel-ml

## show kernel packages

- This command will show all available kernel packages from the elrepo repository
    - ```yum list available --disablerepo=* --enablerepo=elrepo-kernel  --showduplicates```


## show installed kernel packages

- This comand will list all kernels installed by filtering out kernels
    - ```yum list installed --showduplicates kernel```

```shell
Loaded plugins: fastestmirror
    Installed Packages
    kernel.x86_64    2.6.32-573.el6         @anaconda-CentOS-201508042137.x86_64/6.7
    kernel.x86_64    2.6.32-573.3.1.el6     @updates
    kernel.x86_64    2.6.32-754.28.1.el6    @updates
```

## Show available kernels that can be installed

```yum list --showduplicates kernel```

```shell

    Loaded plugins: fastestmirror
    Loading mirror speeds from cached hostfile
     * base: mirror.dst.ca
     * epel: d2lzkl7pfhq30w.cloudfront.net
     * extras: mirror.netflash.net
     * updates: mirror.netflash.net
    Installed Packages
    kernel.x86_64    2.6.32-573.el6         @anaconda-CentOS-201508042137.x86_64/6.7
    kernel.x86_64    2.6.32-573.3.1.el6     @updates
    kernel.x86_64    2.6.32-754.28.1.el6    @updates
    Available Packages
    kernel.x86_64    2.6.32-754.el6         base
    kernel.x86_64    2.6.32-754.2.1.el6     updates
    kernel.x86_64    2.6.32-754.3.5.el6     updates
    kernel.x86_64    2.6.32-754.6.3.el6     updates
    kernel.x86_64    2.6.32-754.9.1.el6     updates
    kernel.x86_64    2.6.32-754.10.1.el6    updates
    kernel.x86_64    2.6.32-754.11.1.el6    updates
    kernel.x86_64    2.6.32-754.12.1.el6    updates
    kernel.x86_64    2.6.32-754.14.2.el6    updates
    kernel.x86_64    2.6.32-754.15.3.el6    updates
    kernel.x86_64    2.6.32-754.17.1.el6    updates
    kernel.x86_64    2.6.32-754.18.2.el6    updates
    kernel.x86_64    2.6.32-754.22.1.el6    updates
    kernel.x86_64    2.6.32-754.23.1.el6    updates
    kernel.x86_64    2.6.32-754.24.2.el6    updates
    kernel.x86_64    2.6.32-754.24.3.el6    updates
    kernel.x86_64    2.6.32-754.25.1.el6    updates
    kernel.x86_64    2.6.32-754.27.1.el6    updates
    kernel.x86_64    2.6.32-754.28.1.el6    updates
```

### Links
- [Archive kernels](http://mirror.cedia.org.ec/elrepo/archive/kernel/el7/x86_64/RPMS/)
