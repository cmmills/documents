# Using Gitlab CLI

## Create a repo

```glab repo create docs6/glabcli```

## Merge requests

List merge requests assigned to me: 
```glab mr list --assignee=@me```   

List review requests for me: 
```glab mr list --reviewer=@me```

Approve a merge request:
```glab mr approve 235```

Create an issue, and add milestone, title, and label: 
```glab issue create -m release-2.0.0 -t "My title here" --label important```