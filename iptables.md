# IPTable

The following rules need to be captilasied.
INPUT = packets coming into the server
FORWARD = packets passing through the server (multi-home system - router)
OUTPUT  = packets leaving the the server

```shell
-s = source address
-d = destination address
-p  = protocol
-j  = action
-P  = specify default policy for a chain
-D  = delete a rule for a chain
-R  = replace a rule for a chain
-F  = remove all rules for a specified chain
-L  = list chain rules
-A  = append/ add rule to end of a chain

--line-numbers  = add line numbers to the display output
```

Add a rules

```/sbin/iptables -I INPUT 1 -j ACCEPT -p tcp --destination-port 80 -i eth0```

Catch-all rule - needs to applied last.

```iptables -A INPUT -j DROP -p tcp -i eth0```

Delete a rule

```iptables -D INPUT 1"```

Delete all rules

```iptables -F```

Flush all rules, - delete all chains and accept all (Disable the fiewall)

```shell
iptables -P INPUT ACCEPT
iptables -p FORWARD ACCEPT
iptables -P OUTPUT ACCEPT
```

Save iptables rules to a file

```iptables-save > /root/iptables-works-`date +%F```

Restore iptables

```iptables-restore < /root/iptables-works-"2016-04-21"```

Monitor ip table rules

```watch --interval=5 "iptables -nvL | grep -v "0     )""```

Create firewall reports

```http://fwreport.sourceforge.net/```
